package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 10/10/2015.
 */
public class EPSelector implements ParentSelection {

	private List<Member> parents;

	private List<Member> offsprings;

	public EPSelector () {
		parents = new ArrayList<Member>();
		offsprings = new ArrayList<Member>();
	}

	@Override
	public void selectParentsFrom (List<Member> members) {
		parents = members;
	}

	public List<Member> getParents () {
		return parents;
	}

	@Override
	public void performRecombinition () {

	}

	@Override
	public void performMutation () {
		for (Member member : parents) {
			Member mutant = member.gaussianMutation();
			mutant.calculateFitness();
			offsprings.add( mutant );
		}
	}

	@Override
	public List<Member> getPopulation () {
		List<Member> population = new ArrayList<Member>( parents );
		population.addAll( offsprings );
		return population;
	}

	@Override
	public List<Member> getOffsprings () {
		return offsprings;
	}
}
