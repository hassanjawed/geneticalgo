package com.ci.geneticalgo.tictactoe;

import com.ci.geneticalgo.Utils;
import com.ci.geneticalgo.functions.TicTacBot;
import com.ci.geneticalgo.neural_network.NeuralNet;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 11/23/2015.
 *
 * Evaluates fitness of two individual players by offering a series of match between both
 */
public class TicTacController {

	public static final int WIN_REWARD   = 1;
	public static final int LOSE_PENALTY = -2;
	private int turnChances = 1;
	private TicTacGame game;
	private NeuralNet  neuralNet;

	public TicTacController (NeuralNet neuralNet) {
		this.neuralNet = neuralNet;
	}

	public void setTurnChances (int turnChances) {
		this.turnChances = turnChances;
	}

	public TicTacGame versusHuman (TicTacBot bot) throws IOException {
		game = new TicTacGame();
		int turnCount = 0;
		TicTacGame.Status status = TicTacGame.Status.NONE;
		while (turnCount < 9 && status != TicTacGame.Status.ENDED) {
			status = progressGame( true, bot );
			turnCount += 1;
			System.out.println( game.log() );
			if (status != TicTacGame.Status.ENDED) {
				int turnIndex;
				while (true) {
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					System.out.print("Enter index between 1-9:");
					int readInput = br.read();
					String input = String.format( "%c", readInput );
					try {
						turnIndex = input != null && input.length() > 0 ? Integer.parseInt( input ) - 1 : -1;
					}
					catch (NumberFormatException e) {
						continue;
					}
					if (!game.isValidTurn(turnIndex)) {
						continue;
					}
					status = game.player2Turn(turnIndex);
					turnCount += 1;
					break;
				}
			}
		}
		System.out.println( "\n---- GAME OVER ----" );
		if (game.getResult() == TicTacGame.Result.P1_WON) {
			System.out.println("Bot won, in " + turnCount + " turns");
		} else if (game.getResult() == TicTacGame.Result.P2_WON) {
			System.out.println("Bot lost, in " + turnCount + " turns");
		} else {
			System.out.println("Game is draw");
		}
		System.out.println(game.log());
		return game;
	}

	/**
	 * Evaluates the given bot by competing it against random player
	 * @param bot trained bot to compete
	 */
	public TicTacGame evaluateBot (TicTacBot bot) {
		game = new TicTacGame();
		int turnCount = 0;
		TicTacGame.Status status = TicTacGame.Status.NONE;
		while (turnCount < 9 && status != TicTacGame.Status.ENDED) {
			status = progressGame( true, bot );
			turnCount += 1;

			if (status != TicTacGame.Status.ENDED) {
				int turnIndex;
				while (!game.isValidTurn(turnIndex = Utils.randomIntBetween(0, 9))) {
				}
				status = game.player2Turn(turnIndex);
				turnCount += 1;
			}
		}

		System.out.println( "\n---- GAME OVER ----" );
		if (game.getResult() == TicTacGame.Result.P1_WON) {
			System.out.println("Bot won, in " + turnCount + " turns");
		} else if (game.getResult() == TicTacGame.Result.P2_WON) {
			System.out.println("Bot lost, in " + turnCount + " turns");
		} else {
			System.out.println("Game is draw");
		}
		System.out.println(game.log());
		return game;
	}

	public TicTacBot evaluateFitness (TicTacBot bot1, TicTacBot bot2) {
		game = new TicTacGame();
		boolean isBot1Turn = true;
		int turnCount = 0;
		TicTacGame.Status status = TicTacGame.Status.NONE;
		while (turnCount < 9 && status != TicTacGame.Status.ENDED) {
			status = progressGame( isBot1Turn, isBot1Turn ? bot1 : bot2 );
			isBot1Turn = !isBot1Turn;
			turnCount++;
		}

		// TODO check for draw case
		if (game.getResult() == TicTacGame.Result.P1_WON) {
			bot1.setFitness( bot1.getFitness() + WIN_REWARD );
			bot2.setFitness( bot2.getFitness() + LOSE_PENALTY );
			return bot1;
		} else {
			bot1.setFitness( bot1.getFitness() + LOSE_PENALTY );
			bot2.setFitness( bot2.getFitness() + WIN_REWARD );
			return bot2;
		}
	}

	private TicTacGame.Status progressGame (boolean isPlayer1turn, TicTacBot bot) {
		bot.setHasPlayed(true);
		List<Float> inputs = new ArrayList<>();
		for (int i : game.state) {
			inputs.add( normalize( i ) );
		}

		int turnIndex = 0;
		int chance = 0;
		while (chance <= turnChances) {
			List<Float> bot1Turn = neuralNet.produceOutput( inputs, bot.getWeights() );
			turnIndex = getTurnIndex( bot1Turn );
			if (game.isValidTurn( turnIndex )) break;
			chance += 1;
		}

		while (!game.isValidTurn(turnIndex = Utils.randomIntBetween(0, 9))) {

		}

		if (!game.isValidTurn( turnIndex )) throw new IllegalStateException( "Chances ran out, bot unable to play correct turn" );

		return isPlayer1turn ? game.player1Turn( turnIndex ) : game.player2Turn( turnIndex );
	}

	private int getTurnIndex (List<Float> turns) {
		int index = 0;
		float largest = deNormalize( turns.get( index ) );
		for (int i = 1; i < turns.size(); i++) {
			float denorm = deNormalize( turns.get( i ) );
			if (denorm > largest) {
				largest = deNormalize( turns.get( i ) );
				index = i;
			}
		}
		return index;
	}

	private int getNearestTurnIndex (List<Float> turns, int target) {
		int index = 0;
		float smallestDistance = (float) Math.sqrt(Utils.square(turns.get(0) * 2 - target));
		for (int i = 1; i < turns.size(); i++) {
			float denorm = deNormalize( turns.get( i ) );
			float distance = (float) Math.sqrt(Utils.square(denorm - target));
			if (smallestDistance > distance) {
				smallestDistance = distance;
				index = i;
			}
		}
		return index;
	}

	private float normalize (float f) {
		return (f + 1f) / 2f;
	}

	private float deNormalize (float f) {
		return f * 2f - 1f;
	}
}
