package com.ci.geneticalgo.tictactoe;

/**
 * Created by Trikster on 11/23/2015.
 */
public class TicTacGame {

	public static final int MAX_WRONG_TURNS = 5;

	int[] state            = new int[9];
	int   p1WrongTurnCount = 0;
	int   p2WrongTurnCount = 0;
	int   turnCount        = 1;
	Result result;

	public TicTacGame () {
		for (int i = 0; i < 9; i++) {
			state[i] = 0;
		}
	}

	public Result getResult () {
		return result;
	}

	public boolean isValidTurn (int index) {
		return isValidIndex( index ) && state[index] == 0;
	}

	public Status player1Turn (int index) {
		if (!isValidIndex( index )) {
			throw new IllegalArgumentException( "Illegal index for turn " + index );
		}

		if (!isValidTurn(index)) return Status.NONE;

		state[index] = 1;
		turnCount += 1;
		if (checkEndGame( 1 )) {
			result = Result.P1_WON;
			return Status.ENDED;
		} else if (turnCount > 9) {
			result = Result.DRAW;
			return Status.ENDED;
		}

		return Status.NONE;
	}

	public Status player2Turn (int index) {
		if (!isValidIndex( index )) {
			throw new IllegalArgumentException( "Illegal index for turn " + index );
		}

		if (!isValidTurn(index)) return Status.NONE;

		state[index] = -1;
		turnCount += 1;
		if (checkEndGame( -1 )) {
			result = Result.P2_WON;
			return Status.ENDED;
		} else if (turnCount > 9) {
			result = Result.DRAW;
			return Status.ENDED;
		}

		return Status.NONE;
	}

	private boolean isValidIndex (int index) {
		return index >= 0 && index < 9;
	}

	private boolean checkEndGame (int foundTurn) {
		return (foundTurn == state[0]) && (foundTurn == state[1]) && (foundTurn == state[2]) ||
				(foundTurn == state[3]) && (foundTurn == state[4]) && (foundTurn == state[5]) ||
				(foundTurn == state[6]) && (foundTurn == state[7]) && (foundTurn == state[8]) ||
				(foundTurn == state[0]) && (foundTurn == state[3]) && (foundTurn == state[6]) ||
				(foundTurn == state[1]) && (foundTurn == state[4]) && (foundTurn == state[7]) ||
				(foundTurn == state[2]) && (foundTurn == state[5]) && (foundTurn == state[8]) ||
				(foundTurn == state[0]) && (foundTurn == state[4]) && (foundTurn == state[8]) ||
				(foundTurn == state[2]) && (foundTurn == state[4]) && (foundTurn == state[6]);
	}

	public String log() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < state.length; i++) {
			if (i != 0 && i % 3 == 0) {
				sb.append( "\n" );
			}
			String s = state[i] == 1 ? "X" : state[i] == -1 ? "O" : "_"; // X for P1 and O for P2
			sb.append( s ).append( "    " );
		}
		return sb.toString();
	}

	enum Status {NONE, ENDED,}

	public enum Result {DRAW, P1_WON, P2_WON,}
}
