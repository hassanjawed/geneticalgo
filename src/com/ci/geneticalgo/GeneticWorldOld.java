package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.*;

/**
 * Created by Trikster on 9/5/2015.
 */
public class GeneticWorldOld {

	private final int          populationSize;
	private       List<Member> population;
	private       Member       bestSoFar;
	private       double       averageFitness;

	public GeneticWorldOld (int populationSize) {
		this.populationSize = populationSize;
		population = new ArrayList<Member>();
	}

	public List<Member> getPopulation () {
		return population;
	}

	public void reinit () {
		for (Member member : population) {
			member.newMember();
		}
		bestSoFar = null;
		averageFitness = 0;
	}

	public Member getBestSoFar () {
		return bestSoFar;
	}

	public double getAverageFitness () {
		return averageFitness;
	}

	public void add (Member individual) {
		population.add( individual );
	}

	public void addAll (List<Member> members) {
		population.addAll( members );
	}

	public void calculateFitness () {
		for (Member member : population) {
			member.calculateFitness();
		}
	}

	public void selectParents (ParentSelection parentSelector) {
		parentSelector.selectParentsFrom( population );
	}

	public List<Member> produceOffspringsFrom (Member parent1, Member parent2) {
		List<Member> offsprings = parent1.recombine( parent2 );

		int mutateSize = (int) (populationSize * 0.1);
		for (int i = 0; i < mutateSize; i++) {
			int index = Utils.randomIntBetween( 0, populationSize );
			offsprings.add( population.get( index ).mutate() );
		}

//		offsprings.add( parent2.mutate() );

		for (Member m : offsprings) {
			m.calculateFitness();
		}
//		for (int i = 0; i < population.size(); i++) {
//			Member m = population.get( i ).mutate();
//			m.calculateFitness();
//			offsprings.add( m );
//		}
//		for (int i = 0; i < (populationSize - 2) / 2; i++) {
//			offsprings.add( offsprings.get( 0 ).mutate() );
//		}
//		for (int i = 0; i < (populationSize - 2) / 2; i++) {
//			offsprings.add( offsprings.get( 1 ).mutate() );
//		}
		return offsprings;
	}

	public void survive (SurvivalSelection selector, List<Member> offsprings, int populationSize) {
		population = new ArrayList<Member>( selector.postSelection( population, offsprings, populationSize ) );
	}

	public Member calculateBestSoFar () {
		ArrayList<Member> temp = new ArrayList<Member>( population );
		Collections.sort( temp );
		bestSoFar = temp.get( temp.size() - 1 );
		return bestSoFar;
	}

	public double calculateAverageFitness () {
		double sum = 0;
		for (Member member : population) {
			sum += member.getFitness();
		}
		averageFitness = sum / population.size();
		return averageFitness;
	}

	public void checkPopulationSize () {
		if (population.size() > populationSize) {
			throw new IllegalStateException( "Population size " + population
					.size() + " exceeds range " + populationSize );

		}
	}

	public void checkMemberValues () {
		for (Member member : population) {
			member.checkValues();
		}
	}
}
