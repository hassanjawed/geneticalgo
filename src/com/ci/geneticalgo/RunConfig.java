package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 9/5/2015.
 */
public class RunConfig {

	private final   int               generations;
	protected final GeneticWorldOld   pointGeneticWorld;
	private final   ParentSelection   parentSelector;
	private final   SurvivalSelection survivalSelector;
	private final   int               populationSize;
	protected       List<Double>      averageBestSoFar;
	protected       List<Double>      averageFitnessAllRun;
	protected       Member            bestSoFar;


	public RunConfig (int generations, int populationSize, GeneticWorldOld pointGeneticWorld, ParentSelection parentSelector,
					  SurvivalSelection
							  survivalSelector) {
		this.generations = generations;
		this.populationSize = populationSize;
		this.pointGeneticWorld = pointGeneticWorld;
		this.parentSelector = parentSelector;
		this.survivalSelector = survivalSelector;
		averageBestSoFar = new ArrayList<Double>();
		averageFitnessAllRun = new ArrayList<Double>();
	}

	public void run (int iterations) {
		for (int i = 0; i < generations; i++) {
			averageBestSoFar.add( 0d );
			averageFitnessAllRun.add( 0d );
		}
		for (int i = 0; i < iterations; i++) {
			run();
			double sum = 0;


			pointGeneticWorld.reinit();
		}
		for (int i = 0; i < generations; i++) {
			Double sum = averageFitnessAllRun.get( i );
//			System.out.println( "Avg. avg. fitness" + (sum / iterations) );
			averageFitnessAllRun.set( i, sum / iterations );

			Double sumBestSoFar = averageBestSoFar.get( i );
			averageBestSoFar.set( i, sumBestSoFar / iterations );

			System.out.println( "ABSF:" + averageBestSoFar.get( i ) + " Avg. Avg. Fitness:" + averageFitnessAllRun
					.get( i ) );
		}
	}

	public void run () {
		for (int i = 0; i < generations; i++) {
			pointGeneticWorld.calculateFitness();
			pointGeneticWorld.selectParents( parentSelector );

//			List<Member> offsprings = pointGeneticWorld
//					.produceOffspringsFrom( parentSelector.getParent1(), parentSelector.getParent2() );

			parentSelector.performRecombinition();
			parentSelector.performMutation();

			List<Member> offsprings = parentSelector.getOffsprings();


			pointGeneticWorld.survive( survivalSelector, offsprings, populationSize );
//			pointGeneticWorld.checkPopulationSize();
			pointGeneticWorld.checkMemberValues();
			bestSoFar = pointGeneticWorld.calculateBestSoFar();

			double avgFitness = averageFitnessAllRun.get( i ) + pointGeneticWorld.calculateAverageFitness();
			averageFitnessAllRun.set( i, avgFitness );

			double sumBestSoFar = averageBestSoFar.get( i ) + bestSoFar.getFitness();
			averageBestSoFar.set( i, sumBestSoFar );

			System.out.println( "Run:" + (i + 1) + " sumBSF:" + sumBestSoFar + " ->BSF:" + bestSoFar.log() );

			if (terminateCondition( i, bestSoFar )) {
				break;
			}
		}
	}

	protected boolean terminateCondition (int iterationCount, Member member) {
		return false;
	}

	public Member getBestSoFar () {
		return bestSoFar;
	}

	public List<Double> getAverageBestSoFar () {
		return averageBestSoFar;
	}

	public List<Double> getAverageFitnessAllRun () {
		return averageFitnessAllRun;
	}
}
