package com.ci.geneticalgo.main;

import com.ci.geneticalgo.*;
import com.ci.geneticalgo.functions.Member;
import com.ci.geneticalgo.functions.PointF2;

import java.util.List;

/**
 * Created by Trikster on 9/7/2015.
 */
public class MainF2 {

	public static void main (String[] args) {
		int populationSize = 10;
		GeneticWorldOld pointGeneticWorld = new GeneticWorldOld( populationSize );
		for (int i = 0; i < populationSize; i++) {
			pointGeneticWorld.add( PointF2.initRandom() );
		}
		for (int i = 0; i < 40; i++) {
			pointGeneticWorld.calculateFitness();
			//            FitnessProportionSelection parentSelector = new FitnessProportionSelection();
			ParentSelection parentSelector = new BinaryTournament();
			pointGeneticWorld.selectParents( parentSelector );
			List<Member> offsprings = parentSelector.getOffsprings();
//			pointGeneticWorld.addAll( offsprings );
//			pointGeneticWorld.calculateFitness();
			pointGeneticWorld.survive( new BinaryTournament(), offsprings, populationSize );
			pointGeneticWorld.checkPopulationSize();
			Member member = pointGeneticWorld.calculateBestSoFar();
			double averageFitness = pointGeneticWorld.calculateAverageFitness();
			if (i >= 35) {
				System.out.println( "Run:" + (i + 1) + " ->BSF:" + member.log() + " AverageFit:" + averageFitness );
			} else {
				System.out.println( "Run:" + (i + 1) + " ->BSF:" + member.log() + " AverageFit:" + averageFitness );
			}
		}
	}

}
