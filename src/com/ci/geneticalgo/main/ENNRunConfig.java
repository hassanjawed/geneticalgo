package com.ci.geneticalgo.main;

import com.ci.geneticalgo.*;
import com.ci.geneticalgo.functions.Member;
import com.ci.geneticalgo.functions.StockMarketFunction;

/**
 * Created by Trikster on 11/1/2015.
 */
public class ENNRunConfig extends RunConfig {

	private static float sseThreshold     = 1000f;
	private static int   convergenceLimit = 5; // Terminate after 5 consecutive SSE in threshold

	private double lastFitness = 0;
	private int convergenceCount;

	public static void setSseThreshold (float sseThreshold) {
		ENNRunConfig.sseThreshold = sseThreshold;
	}

	public static void setConvergenceLimit (int convergenceLimit) {
		ENNRunConfig.convergenceLimit = convergenceLimit;
	}

	public ENNRunConfig (int generations, int populationSize, GeneticWorldOld pointGeneticWorld, ParentSelection parentSelector, SurvivalSelection survivalSelector) {
		super( generations, populationSize, pointGeneticWorld, parentSelector, survivalSelector );
	}

	@Override
	protected boolean terminateCondition (int iterationCount, Member member) {
		if (Math.abs( member.getFitness() ) <= 0.5f) {
			StockMarketFunction.nextInput();
		}
		convergenceCount = ( member.getFitness() - lastFitness ) <= sseThreshold ? convergenceCount + 1 : 0;
		lastFitness = member.getFitness();
		return StockMarketFunction.haveLearnt() || convergenceCount >= convergenceLimit;

	}
}
