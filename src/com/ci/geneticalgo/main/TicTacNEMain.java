package com.ci.geneticalgo.main;

import com.ci.geneticalgo.functions.TicTacBot;
import com.ci.geneticalgo.neural_network.NeuralNet;
import com.ci.geneticalgo.tictactoe.TicTacController;
import com.ci.geneticalgo.tictactoe.TicTacGame;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Trikster on 11/26/2015.
 */
public class TicTacNEMain extends Application {

	public static final int NEURON_COUNT = 8;


	String weights = "-0.6839754, -1.0779624, -1.4840528, -0.93814963, -0.39790106, -1.386017, 1.4330822, 1.2711076, -1.0323983, -0.25884902, 0.15099017, -0.13622805, 0.28938073, -0.32367888, -1.8327994, -1.1909189, -0.9254313, 1.148564, -1.4198611, 0.48756227, 1.3783535, 1.8541778, 1.9868649, 1.9143374, -0.3385483, 1.3710057, 1.8860036, 1.4101756, 1.3039511, -1.4707103, -0.18432605, -0.5041296, -0.95037794, 0.92144835, -1.6136194, 1.2026669, 0.43239197, -1.1133646, -1.2915175, 1.0528842, 1.6277742, 1.8303992, -0.4661166, -0.4422064, -1.0529966, 1.4139273, -1.4559207, 0.45303407, -1.2602724, 0.12694599, 1.9992236, 1.454817, -1.4579571, -0.23418665, 1.9292485, -1.7257146, -1.6032537, 1.1414506, -0.14109108, -1.3331629, 0.19376142, 1.3322436, -1.1448815, -1.4546567, -1.5848112, -1.5453826, 0.570623, -0.7854861, -1.6206306, -0.27022517, -0.14856865, -1.0766139, 0.37259376, -0.7612375, -1.2621957, 0.2512211, -0.1859199, -0.85259646, 0.0145878475, 0.6993227, -1.7639867, -1.2476358, -0.08535217, -0.6025828, -0.030898673, 1.7409347, 0.2693915, 1.9243025, -0.53299224, -1.6081098, -0.063548364, -0.3798051, 0.9033745, 1.5028027, 1.704076, 1.1875954, 1.6511725, 0.7313674, 0.67459506, 1.3829591, 0.33214125, 1.1018021, 0.56848705, -1.9004261, -0.5627231, 1.3561289, 0.6415445, 1.3671589, -1.9902974, 0.552806, 1.7865542, 1.0788558, 0.822659, 0.16282877, 1.394808, -0.3335672, -1.4482372, 1.8860813, -1.318333, -0.5203117, -1.3679186, 0.5083713, -0.56569046, -1.7138137, 0.8069856, 0.34223896, -0.48129907, -1.9073766, -1.1828579, -0.8577117, -0.013276204, 0.3992715, -1.7555826, 0.85844326, -0.7466796, 1.0341086, -1.7831311, -1.811812, -0.4325988, 1.8733956, -0.50259286, -0.38849255, -0.12081991, 1.04062";

	public static void main (String[] args) {
		launch( args );
	}

	@Override
	public void start (Stage primaryStage) throws Exception {
		String[] weights = this.weights.split( ", " );
		TicTacBot.setNeuronCount( 9, NEURON_COUNT, 9 );
		TicTacBot bot = new TicTacBot();
		bot = (TicTacBot) bot.newMember();
		for (int i = 0; i < weights.length; i++) {
			bot.getWeights().set( i, Float.parseFloat( weights[i] ) );
		}
		NeuralNet neuralNet = new NeuralNet( 9, NEURON_COUNT, 9 );
		neuralNet.buildNet();
		TicTacController controller = new TicTacController( neuralNet );

		for (int i = 0; i < bot.getWeights().size(); i++) {
			Float weight = bot.getWeights().get( i );
			float distance = Math.abs( weight );
			if (distance < 0.1f) {
				bot.getWeights().set( i, 0f );
			}
		}
//		System.out.println(bot.log()+"\n-------------------------------------------");
		int p1WinCount = 0, p2WinCount = 0, noResult = 0;
		for (int i = 0; i < 5; i++) {
			TicTacGame ticTacGame = controller.evaluateBot( bot );
			if (ticTacGame.getResult() == TicTacGame.Result.P1_WON) {
				p1WinCount += 1;
			} else if (ticTacGame.getResult() == TicTacGame.Result.P2_WON) {
				p2WinCount += 1;
			} else {
				noResult += 1;
			}
			BufferedReader br = new BufferedReader( new InputStreamReader( System.in ) );
			System.out.println( "Bot vs random: Win:"
					+ p1WinCount
					+ " Lost:"
					+ p2WinCount
					+ " NR:"
					+ noResult );
		}
//		System.out.println(
//				"You are playing with winner of " + (bot.getFitness() - 50) + " games");
		p1WinCount = 0;
		p2WinCount = 0;
		noResult = 0;
		while (true) {
			TicTacGame ticTacGame = controller.versusHuman( bot );
			if (ticTacGame.getResult() == TicTacGame.Result.P1_WON) {
				p1WinCount += 1;
			} else if (ticTacGame.getResult() == TicTacGame.Result.P2_WON) {
				p2WinCount += 1;
			} else {
				noResult += 1;
			}
			BufferedReader br = new BufferedReader( new InputStreamReader( System.in ) );
			System.out.print( "Bot results: Win:"
					+ p1WinCount
					+ " Lost:"
					+ p2WinCount
					+ " NR:"
					+ noResult
					+ "\nEnter y to play again:" );
			if (String.format( "%c", br.read() ).equalsIgnoreCase( "y" )) {
				continue;
			} else if (String.format( "%c", br.read() ).equalsIgnoreCase( "q" )) {
				System.exit( 0 );
			} else {
				break;
			}
		}
	}
}
