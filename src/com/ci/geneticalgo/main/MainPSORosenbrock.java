package com.ci.geneticalgo.main;

import com.ci.geneticalgo.*;
import com.ci.geneticalgo.functions.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 10/24/2015.
 */
public class MainPSORosenbrock extends BaseApplication {

	public static void main (String[] args) {
		launch( args );
	}

	@Override
	protected Member initMember () {
		return RosenbrockFunction.init();
	}

	@Override
	protected List<Selectors> getSelectors () {
		List<Selectors> selectors = new ArrayList<>();

		selectors.add( new Selectors( new ParticleSwarmOptimizer(), new ParticleSwarmOptimizer() ) );
//		selectors.add( new Selectors( new FitnessProportionSelection(), new Truncation() ) );
		return selectors;
	}

	@Override
	protected String getFunctionName () {
		return "Rosenbrock";
	}

	@Override
	protected String getImagePrefix () {
		return "4";
	}

	@Override
	protected boolean captureScreenshots () {
		return false;
	}
}
