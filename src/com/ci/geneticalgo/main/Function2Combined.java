package com.ci.geneticalgo.main;

import com.ci.geneticalgo.*;
import com.ci.geneticalgo.functions.PointF2;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.*;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

/**
 * Created by Trikster on 9/18/2015.
 */
public class Function2Combined extends Application {

	public static void main (String[] args) {
		launch( args );
	}

	@Override
	public void start (Stage primaryStage) throws Exception {
		List<DataHolder> data = new ArrayList<DataHolder>();
		List<ParentSelection> selectors = new ArrayList<ParentSelection>();
		selectors.add( new FitnessProportionSelection() );
		selectors.add( new RankBasedSelection() );
		selectors.add( new BinaryTournament() );

		List<SurvivalSelection> survivalSelections = new ArrayList<SurvivalSelection>();
		survivalSelections.add( new Truncation() );
		survivalSelections.add( new BinaryTournament() );

		for (ParentSelection p : selectors) {
			for (SurvivalSelection s : survivalSelections) {
				int populationSize = 10;
				GeneticWorldOld pointGeneticWorld = new GeneticWorldOld( populationSize );
				for (int i = 0; i < 10; i++) {
					pointGeneticWorld.add( PointF2.initRandom() );
				}
				RunConfig runConfig = new RunConfig( 40, 10, pointGeneticWorld, p, s );
				runConfig.run( 10 );

				final String selectionAlgoName = p.getClass().getSimpleName();
				final String survivalAlgoName = s.getClass().getSimpleName();

				int i = 1;
				XYChart.Series avgBSFSeries = new XYChart.Series();
				XYChart.Series avgBSFSeries2 = new XYChart.Series();
				avgBSFSeries.setName( selectionAlgoName + "_" + survivalAlgoName );
				avgBSFSeries2.setName( selectionAlgoName + "_" + survivalAlgoName );
				for (Double v : runConfig.getAverageBestSoFar()) {
					avgBSFSeries.getData().add( new XYChart.Data( i++, v ) );
					avgBSFSeries2.getData().add( new XYChart.Data( i++, v ) );
				}

				XYChart.Series avgAvgFitnessSeries = new XYChart.Series();
				XYChart.Series avgAvgFitnessSeries2 = new XYChart.Series();
				avgAvgFitnessSeries.setName( selectionAlgoName + "_" + survivalAlgoName );
				avgAvgFitnessSeries2.setName( selectionAlgoName + "_" + survivalAlgoName );
				i = 1;
				for (Double a : runConfig.getAverageFitnessAllRun()) {
					avgAvgFitnessSeries.getData().add( new XYChart.Data( i++, a ) );
					avgAvgFitnessSeries2.getData().add( new XYChart.Data( i++, a ) );
				}

				DataHolder holder = new DataHolder( selectionAlgoName, survivalAlgoName, avgBSFSeries, avgAvgFitnessSeries );
				holder.avgBSF2 = avgBSFSeries2;
				holder.avgAvgFitness2 = avgAvgFitnessSeries2;
				data.add( holder );
			}
		}

		List<XYChart.Series> avgBSFCombined = new ArrayList<XYChart.Series>();
		List<XYChart.Series> avgAvgFitnessCombined = new ArrayList<XYChart.Series>();
		XYChart.Series[] avgBSFAllSeries = new XYChart.Series[data.size()];
		XYChart.Series[] avgAvgFitnessAllSeries = new XYChart.Series[data.size()];

		for (int i = 0; i < data.size(); i++) {
			avgBSFCombined.add( data.get( i ).avgBSF );
			avgAvgFitnessCombined.add( data.get( i ).avgAvgFitness );
		}

		for (DataHolder dataHolder : data) {
			Stage stage = new Stage( StageStyle.DECORATED );
			String algoName = dataHolder.parentSelectionName + "_" + dataHolder.survivalSelectionName;
			final LineChart<Number, Number> chartAlgos = showGraph( stage, "Function 2: " + algoName, "", dataHolder.avgBSF2,
					dataHolder.avgAvgFitness2 );
			takeScreenshot( "func2_" + algoName, chartAlgos );
		}

		Stage stage1 = new Stage( StageStyle.DECORATED );
		final LineChart<Number, Number> chartAvgBSF = showGraph( stage1, "Function 2: Average BSF", "",
				avgBSFCombined.toArray( avgBSFAllSeries ) );

		Stage stage2 = new Stage( StageStyle.DECORATED );
		final LineChart<Number, Number> chartAvgAvgFitness = showGraph( stage2, "Function 2: Average Average Fitness", "",
				avgAvgFitnessCombined.toArray( avgAvgFitnessAllSeries ) );

		takeScreenshot( "func2_average_bsf",chartAvgBSF );
		takeScreenshot( "func2_average_avg_fitness",chartAvgAvgFitness );
	}

	private void takeScreenshot (final String fileName, final LineChart<Number, Number> lineChart) {
		Timeline fiveSecondsWonder = new Timeline( new KeyFrame( Duration
				.seconds( 2 ), new EventHandler<ActionEvent>() {

			@Override
			public void handle (ActionEvent event) {
				try {
					SnapshotParameters parameters = new SnapshotParameters();
					WritableImage wi = new WritableImage( 800, 600 );
					WritableImage snapshot = lineChart.snapshot( new SnapshotParameters(), wi );

					File output = new File( fileName + ".png" );
					ImageIO.write( SwingFXUtils.fromFXImage( snapshot, null ), "png", output );
				}
				catch (IOException ex) {
					Logger.getLogger( Function1.class.getName() ).log( Level.SEVERE, null, ex );
				}
			}
		} ) );
		fiveSecondsWonder.setCycleCount( 1 );
		fiveSecondsWonder.play();
	}

	private LineChart<Number, Number> showGraph (Stage stage, String graphName, String axisLabel,
												 XYChart.Series... series) {

		stage.setTitle( "Evolutionary Algorithm" );
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		xAxis.setLabel( "Iteration" );
		yAxis.setLabel( axisLabel );

		final LineChart<Number, Number> lineChart =
				new LineChart<Number, Number>( xAxis, yAxis );

		lineChart.setTitle( graphName );

		Scene scene = new Scene( lineChart, 800, 600 );
		lineChart.getData().addAll( series );

		stage.setScene( scene );
		stage.show();
		return lineChart;
	}

	private static class DataHolder {

		String         parentSelectionName;
		String         survivalSelectionName;
		XYChart.Series avgBSF;
		XYChart.Series avgBSF2;
		XYChart.Series avgAvgFitness;
		XYChart.Series avgAvgFitness2;

		public DataHolder (String parentSelectionName, String survivalSelectionName, XYChart.Series avgBSF, XYChart.Series avgAvgFitness) {
			this.parentSelectionName = parentSelectionName;
			this.survivalSelectionName = survivalSelectionName;
			this.avgBSF = avgBSF;
			this.avgAvgFitness = avgAvgFitness;
		}
	}
}
