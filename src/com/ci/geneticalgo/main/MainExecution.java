package com.ci.geneticalgo.main;

import com.ci.geneticalgo.algos.*;
import com.ci.geneticalgo.executors.*;
import com.ci.geneticalgo.functions.*;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.*;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

/**
 * Created by Trikster on 11/6/2015.
 */
public class MainExecution extends Application {

	private static final boolean CAPTURE_SCREENSHOTS = true;

	public static void main (String[] args) {
		launch( args );
	}

	@Override
	public void start (Stage primaryStage) throws Exception {
		startExecution();
	}

	private void startExecution () {
		HimmelblauFunction function = HimmelblauFunction.init();
		List<ResultStore> results = new ArrayList<>();

		List<Member> members = new ArrayList<>();
		members.add( function );
		GeneticWorld geneticWorld = new GeneticWorld( members, 10 );
		GAExecutor.GAConfiguration configuration = new GAExecutor.GAConfiguration( 10, 40, new NFitnessProportionSelector(), new
				StrictTruncation() );
		GAExecutor gaExecutor = new GAExecutor( geneticWorld, configuration );
		ResultStore resultStore = gaExecutor.execute();
		resultStore.setTitle( "FPS + Truncation" );
		resultStore.setImageName( "4_fps_truncation" );
		results.add( resultStore );

		List<PointParticle> population = new ArrayList<>();
		population.add( function );
		ParticleWorld particleWorld = new ParticleWorld( population, 10 );
		PSOExecutor executor = new PSOExecutor( particleWorld, new Executor.Configuration( 10, 40 ) );
		resultStore = executor.execute();
		resultStore.setTitle( "PSO" );
		resultStore.setImageName( "4_pso" );
		results.add( resultStore );

		List<AISMember> aisMembers = new ArrayList<>();
		aisMembers.add( function );
		AISWorld aisWorld = new AISWorld( aisMembers, 10 );
		AISExecutor aisExecutor = new AISExecutor( aisWorld, 40 );
		resultStore = aisExecutor.execute();
		resultStore.setTitle( "AIS" );
		resultStore.setImageName( "4_ais" );
		results.add( resultStore );

		for (ResultStore rs : results) {
			List<XYChart.Series<Number, Float>> series = makeXYChart( rs );
			XYChart.Series[] aisSeries = new XYChart.Series[series.size()];
			series.toArray( aisSeries );
			Stage stage = new Stage( StageStyle.DECORATED );
			LineChart<Number, Number> lineChart = showGraph( stage, rs.getTitle(), "Fitness", aisSeries );
			takeScreenshot( rs.getImageName(), lineChart );
		}
		List<XYChart.Series<Number, Float>> avgBSFSeries = makeAvgBSFSeries( results );
		XYChart.Series[] bsfSeries = new XYChart.Series[avgBSFSeries.size()];
		avgBSFSeries.toArray( bsfSeries );
		Stage bsfStage = new Stage( StageStyle.DECORATED );
		LineChart<Number, Number> bsfChart = showGraph( bsfStage, "Average BSF", "Fitness", bsfSeries );
		takeScreenshot( "4_bsf",bsfChart );

		List<XYChart.Series<Number, Float>> avgFitnessSeries = makeAvgFitnessSeries( results );
		XYChart.Series[] fitnessSeries = new XYChart.Series[avgFitnessSeries.size()];
		avgFitnessSeries.toArray( fitnessSeries );
		Stage fitnessStage = new Stage( StageStyle.DECORATED );
		LineChart<Number, Number> fitnessChart = showGraph( fitnessStage, "Average Fitness", "Fitness", fitnessSeries );
		takeScreenshot( "4_avg_fitness",fitnessChart );

	}

	private List<XYChart.Series<Number, Float>> makeAvgBSFSeries (List<ResultStore> results) {
		List<XYChart.Series<Number, Float>> allSeries = new ArrayList<>();
		List<List<Float>> allAvgBSF = new ArrayList<>();
		for (int i = 0; i < results.size(); i++) {
			allAvgBSF.add( results.get( i ).calculateAverageBestSoFar() );
			XYChart.Series<Number, Float> series = new XYChart.Series<>();
			series.setName( results.get( i ).getTitle() );
			allSeries.add( series );
			System.out.println( "-------------------- " + results.get( i ).getTitle() + " --------------------" );
			List<Float> avgBSF = allAvgBSF.get( i );
			for (int j = 0; j < avgBSF.size(); j++) {
				System.out.println( j + ": AvgBSF: " + avgBSF.get( j ) );
				series.getData().add( new XYChart.Data<>( j + 1, avgBSF.get( j ) ) );
			}
		}
		return allSeries;
	}

	private List<XYChart.Series<Number, Float>> makeAvgFitnessSeries (List<ResultStore> results) {
		List<XYChart.Series<Number, Float>> allSeries = new ArrayList<>();
		List<List<Float>> allAvgBSF = new ArrayList<>();
		for (int i = 0; i < results.size(); i++) {
			allAvgBSF.add( results.get( i ).calculateAverageAverageFitness() );
			XYChart.Series<Number, Float> series = new XYChart.Series<>();
			series.setName( results.get( i ).getTitle() );
			allSeries.add( series );

			System.out.println( "-------------------- " + results.get( i ).getTitle() + " --------------------" );
			List<Float> avgBSF = allAvgBSF.get( i );
			for (int j = 0; j < avgBSF.size(); j++) {
				System.out.println( j + ": AvgFitness: " + avgBSF.get( j ) );
				series.getData().add( new XYChart.Data<>( j + 1, avgBSF.get( j ) ) );
			}
		}
		return allSeries;
	}

	private List<XYChart.Series<Number, Float>> makeXYChart (ResultStore resultStore) {
		XYChart.Series<Number, Float> avgBSFSeries = new XYChart.Series<>();
		avgBSFSeries.setName( "Avg BSF" );
		XYChart.Series<Number, Float> avgFitnessSeries = new XYChart.Series<>();
		avgFitnessSeries.setName( "Avg Fitness" );
		List<Float> fitness = resultStore.calculateAverageAverageFitness();
		List<Float> bsf = resultStore.calculateAverageBestSoFar();
		for (int i = 0; i < fitness.size(); i++) {
			avgBSFSeries.getData().add( new XYChart.Data<>( i + 1, bsf.get( i ) ) );
			avgFitnessSeries.getData().add( new XYChart.Data<>( i + 1, fitness.get( i ) ) );
		}
		List<XYChart.Series<Number, Float>> allSeries = new ArrayList<>();
		allSeries.add( avgBSFSeries );
		allSeries.add( avgFitnessSeries );
		return allSeries;
	}

	private LineChart<Number, Number> showGraph (Stage stage, String graphName, String axisLabel,
												 XYChart.Series... series) {

		stage.setTitle( "Evolutionary Algorithm" );
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		xAxis.setLabel( "# of generation" );
		yAxis.setLabel( axisLabel );

		final LineChart<Number, Number> lineChart =
				new LineChart<Number, Number>( xAxis, yAxis );

		lineChart.setTitle( graphName );

		Scene scene = new Scene( lineChart, 800, 600 );
		lineChart.getData().addAll( series );

		stage.setScene( scene );
		stage.show();
		return lineChart;
	}

	private void takeScreenshot (final String fileName, final LineChart<Number, Number> lineChart) {
		if (!CAPTURE_SCREENSHOTS) return;

		Timeline fiveSecondsWonder = new Timeline( new KeyFrame( Duration
				.seconds( 2 ), new EventHandler<ActionEvent>() {

			@Override
			public void handle (ActionEvent event) {
				try {
					SnapshotParameters parameters = new SnapshotParameters();
					WritableImage wi = new WritableImage( 800, 600 );
					WritableImage snapshot = lineChart.snapshot( new SnapshotParameters(), wi );

					File output = new File( fileName + ".png" );
					ImageIO.write( SwingFXUtils.fromFXImage( snapshot, null ), "png", output );
				}
				catch (IOException ex) {
					Logger.getLogger( Function1.class.getName() ).log( Level.SEVERE, null, ex );
				}
			}
		} ) );
		fiveSecondsWonder.setCycleCount( 1 );
		fiveSecondsWonder.play();
	}
}
