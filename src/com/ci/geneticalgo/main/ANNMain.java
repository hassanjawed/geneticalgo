package com.ci.geneticalgo.main;

import com.ci.geneticalgo.neural_network.ArtificialNeuralNetwork;
import com.ci.geneticalgo.neural_network.Data;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.*;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.apache.commons.csv.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

/**
 * Created by Trikster on 10/25/2015.
 */
public class ANNMain extends Application {


	public static final int HIDDEN_NEURON_COUNT = 30;
	public static final int EPOCHS              = 900000;

	public static void main (String[] args) throws IOException {
		launch( args );
	}

	@Override
	public void start (Stage primaryStage) throws Exception {
		File csvFile = new File( "D:\\My Data\\MS\\CI\\GeneticAlgo\\src\\com\\ci\\geneticalgo\\data.csv" );
		CSVParser parser = CSVParser.parse( csvFile, Charset.defaultCharset(), CSVFormat.EXCEL );

		List<Data> csvData = new ArrayList<>();
		float max = 0, min = 0;
		for (CSVRecord record : parser) {
			if (parser.getRecordNumber() == 1) continue;
			Data data = new Data();

			for (int i = 0; i < 4; i++) {
				//				System.out.print( record.get( i ) + " | " );
				float v = data.setValue( i, record.get( i ) );
				if (v > max) {
					max = v;
				}
				if (v < min || min == 0) {
					min = v;
				}
			}
			csvData.add( data );
			//			System.out.println();
		}
		long startTime = System.nanoTime();
		float diff = max - min;
		for (Data data : csvData) {
			data.normalize( min, diff );
			System.out.println( data.log() );
		}
		ArtificialNeuralNetwork artificialNeuralNetwork = new ArtificialNeuralNetwork();
		artificialNeuralNetwork.initHiddenLayer( HIDDEN_NEURON_COUNT );
//		List<Data> results = artificialNeuralNetwork.startWith( csvData, 2 );
		artificialNeuralNetwork.train( csvData.subList( 0, (int) (csvData.size() * 0.67f) ), EPOCHS,
				HIDDEN_NEURON_COUNT );
		List<Data> results = artificialNeuralNetwork.test( csvData );

		long endTime = System.nanoTime();
		long elapsedTime = (endTime - startTime) / (1000 * 1000);
		System.out.println( "----------------------------------------------" );
		System.out.println( "Time taken: " + elapsedTime + "secs" );

		XYChart.Series<Number, Float> actual = new XYChart.Series<>();
		XYChart.Series<Number, Float> predicted = new XYChart.Series<>();
		actual.setName( "Actual Y" );
		predicted.setName( "Predicted Y" );
		for (int i = 0; i < results.size(); i++) {
			float y = results.get( i ).y;
			float v = results.get( i ).predictedY();
			actual.getData().add( new XYChart.Data<>( i, y ) );
			predicted.getData().add( new XYChart.Data<>( i, v ) );
			System.out.println( "Actual:" + y + " Predicted:" + v );
		}
		LineChart<Number, Number> chart = showGraph( primaryStage, "ANN", "Y", actual, predicted );
		takeScreenshot( "ann_" + getImageNameParams(), chart );
	}

	private String getImageNameParams () {
		StringBuilder sb = new StringBuilder();
		sb.append( "hnc" ).append( HIDDEN_NEURON_COUNT ).append( "_" );
		sb.append( "e" ).append( EPOCHS );
		return sb.toString();
	}

	private void takeScreenshot (final String fileName, final LineChart<Number, Number> lineChart) {
		if (!captureScreenshots()) return;

		Timeline fiveSecondsWonder = new Timeline( new KeyFrame( Duration
				.seconds( 2 ), new EventHandler<ActionEvent>() {

			@Override
			public void handle (ActionEvent event) {
				try {
					SnapshotParameters parameters = new SnapshotParameters();
					WritableImage wi = new WritableImage( 800, 600 );
					WritableImage snapshot = lineChart.snapshot( new SnapshotParameters(), wi );

					File output = new File( fileName + ".png" );
					ImageIO.write( SwingFXUtils.fromFXImage( snapshot, null ), "png", output );
				}
				catch (IOException ex) {
					Logger.getLogger( Function1.class.getName() ).log( Level.SEVERE, null, ex );
				}
			}
		} ) );
		fiveSecondsWonder.setCycleCount( 1 );
		fiveSecondsWonder.play();
	}

	protected boolean captureScreenshots () {
		return true;
	}

	private LineChart<Number, Number> showGraph (Stage stage, String graphName, String axisLabel,
												 XYChart.Series... series) throws MalformedURLException {
		stage.setTitle( "Artificial Neural Network" );
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		xAxis.setLabel( "x" );
		yAxis.setLabel( axisLabel );

		final LineChart<Number, Number> lineChart =
				new LineChart<>( xAxis, yAxis );

		lineChart.setTitle( graphName );

		Scene scene = new Scene( lineChart, 800, 600 );
		File f = new File( "D:\\My Data\\MS\\CI\\GeneticAlgo\\css\\chart.css" );
		URL url = f.toURI().toURL();
		scene.getStylesheets().clear();
		scene.getStylesheets().add( url.toExternalForm() );
//		scene.getStylesheets().add( "file:///" + f.getAbsolutePath().replace( "\\", "/" ) );
		lineChart.getData().addAll( series );


		stage.setScene( scene );
		stage.show();
		return lineChart;
	}
}
