package com.ci.geneticalgo.main;

import com.ci.geneticalgo.algos.AISWorld;
import com.ci.geneticalgo.executors.AISExecutor;
import com.ci.geneticalgo.executors.ResultStore;
import com.ci.geneticalgo.functions.*;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.*;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

/**
 * Created by Trikster on 11/6/2015.
 */
public class AISMain extends Application {

	private static final boolean CAPTURE_SCREENSHOTS = true;

	public static void main (String[] args) {
		launch( args );
	}

	@Override
	public void start (Stage primaryStage) throws Exception {
		startExecution();
	}

	private void startExecution () {
		List<AISMember> list = new ArrayList<>();
		list.add( HimmelblauFunction.init() );
		AISExecutor executor = new AISExecutor( new AISWorld( list, 10 ), 40 );
		ResultStore resultStore = executor.execute();
		List<XYChart.Series<Number, Float>> series = makeXYChart( resultStore );
		XYChart.Series[] aisSeries = new XYChart.Series[series.size()];
		series.toArray( aisSeries );
		Stage stage = new Stage( StageStyle.DECORATED );
		LineChart<Number, Number> lineChart = showGraph( stage, "AIS", "Fitness", aisSeries );
		takeScreenshot( "4_ais", lineChart );
	}

	private List<XYChart.Series<Number, Float>> makeXYChart (ResultStore resultStore) {
		XYChart.Series<Number, Float> avgBSFSeries = new XYChart.Series<>();
		avgBSFSeries.setName( "Avg BSF" );
		XYChart.Series<Number, Float> avgFitnessSeries = new XYChart.Series<>();
		avgFitnessSeries.setName( "Avg Fitness" );
		List<Float> fitness = resultStore.calculateAverageAverageFitness();
		List<Float> bsf = resultStore.calculateAverageBestSoFar();
		for (int i = 0; i < fitness.size(); i++) {
			avgBSFSeries.getData().add( new XYChart.Data<>( i + 1, bsf.get( i ) ) );
			avgFitnessSeries.getData().add( new XYChart.Data<>( i + 1, fitness.get( i ) ) );
		}
		List<XYChart.Series<Number, Float>> allSeries = new ArrayList<>();
		allSeries.add( avgBSFSeries );
		allSeries.add( avgFitnessSeries );
		return allSeries;
	}

	private LineChart<Number, Number> showGraph (Stage stage, String graphName, String axisLabel,
												 XYChart.Series... series) {

		stage.setTitle( "Evolutionary Algorithm" );
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		xAxis.setLabel( "# of generation" );
		yAxis.setLabel( axisLabel );

		final LineChart<Number, Number> lineChart =
				new LineChart<Number, Number>( xAxis, yAxis );

		lineChart.setTitle( graphName );

		Scene scene = new Scene( lineChart, 800, 600 );
		lineChart.getData().addAll( series );

		stage.setScene( scene );
		stage.show();
		return lineChart;
	}

	private void takeScreenshot (final String fileName, final LineChart<Number, Number> lineChart) {
		if (!CAPTURE_SCREENSHOTS) return;

		Timeline fiveSecondsWonder = new Timeline( new KeyFrame( Duration
				.seconds( 2 ), new EventHandler<ActionEvent>() {

			@Override
			public void handle (ActionEvent event) {
				try {
					SnapshotParameters parameters = new SnapshotParameters();
					WritableImage wi = new WritableImage( 800, 600 );
					WritableImage snapshot = lineChart.snapshot( new SnapshotParameters(), wi );

					File output = new File( fileName + ".png" );
					ImageIO.write( SwingFXUtils.fromFXImage( snapshot, null ), "png", output );
				}
				catch (IOException ex) {
					Logger.getLogger( Function1.class.getName() ).log( Level.SEVERE, null, ex );
				}
			}
		} ) );
		fiveSecondsWonder.setCycleCount( 1 );
		fiveSecondsWonder.play();
	}
}
