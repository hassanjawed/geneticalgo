package com.ci.geneticalgo.main;

import com.ci.geneticalgo.*;
import com.ci.geneticalgo.functions.Quadratic;

public class Main {

    public static void main(String[] args) {
        // write your code here
        int populationSize = 10;
        GeneticWorldOld pointGeneticWorld = new GeneticWorldOld( populationSize );
        for (int i = 0; i < 10; i++) {
            pointGeneticWorld.add( Quadratic.initRandom() );
        }
        ParentSelection parentSelector = new RankBasedSelection();
        SurvivalSelection survivalSelector = new BinaryTournament();
        RunConfig runConfig = new RunConfig( 40, 10, pointGeneticWorld, parentSelector, survivalSelector );
        runConfig.run();
//        int populationSize = 10;
//        GeneticWorldOld pointGeneticWorld = new GeneticWorldOld( populationSize );
//        for (int i = 0; i < populationSize; i++) {
//            pointGeneticWorld.add( Quadratic.initRandom() );
//        }
//        for (int i = 0; i < 40; i++) {
//            pointGeneticWorld.calculateFitness();
////            FitnessProportionSelection parentSelector = new FitnessProportionSelection();
//            ParentSelection parentSelector = new RankBasedSelection();
//            pointGeneticWorld.selectParents( parentSelector );
//            List<Member> offsprings = pointGeneticWorld
//                    .produceOffspringsFrom( parentSelector.getParent1(), parentSelector.getParent2() );
////            pointGeneticWorld.addAll( offsprings );
////            pointGeneticWorld.calculateFitness();
//            pointGeneticWorld.survive( new Truncation(), offsprings, populationSize );
//            pointGeneticWorld.checkPopulationSize();
//            Member member = pointGeneticWorld.calculateBestSoFar();
//            double averageFitness = pointGeneticWorld.calculateAverageFitness();
//            if (i >= 35) {
//                System.out.println( "Run:" + (i + 1) + " ->BSF:" + member.log() + " AverageFit:" + averageFitness );
//            } else {
//                System.out.println( "Run:" + (i + 1) + " ->BSF:" + member.log() + " AverageFit:" + averageFitness );
//            }
//        }
    }
}
