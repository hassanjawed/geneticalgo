package com.ci.geneticalgo.main;

import com.ci.geneticalgo.*;
import com.ci.geneticalgo.functions.Quadratic;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.*;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

/**
 * Created by Trikster on 9/7/2015.
 */
public class Function1 extends Application {


	public static void main (String[] args) {
		launch( args );
	}

	@Override
	public void start (Stage stage) throws Exception {
		System.setProperty( "java.util.Arrays.useLegacyMergeSort", "true" );
		XYChart.Series series = new XYChart.Series();
		series.setName( "Avg. Best so far" );

		int populationSize = 10;
		GeneticWorldOld pointGeneticWorld = new GeneticWorldOld( populationSize );
		for (int i = 0; i < 10; i++) {
			pointGeneticWorld.add( Quadratic.initRandom() );
		}

		ParentSelection parentSelector = new RankBasedSelection();
		SurvivalSelection survivalSelector = new BinaryTournament();
		RunConfig runConfig = new RunConfig( 40, 10, pointGeneticWorld, parentSelector, survivalSelector );
		runConfig.run( 10 );
		int i = 1;
		for (Double v : runConfig.getAverageBestSoFar()) {
			series.getData().add( new XYChart.Data( i++, v ) );
		}
		final String selectionAlgoName = parentSelector.getClass().getSimpleName();
		final String survivalAlgoName = survivalSelector.getClass().getSimpleName();
		String axisLabel = "Average Best so far";
//		showGraph( stage, series, selectionAlgoName, survivalAlgoName, axisLabel );

		XYChart.Series avgSeries = new XYChart.Series();
		avgSeries.setName( "Avg. Avg. Fitness" );
		i = 1;
		for (Double a : runConfig.getAverageFitnessAllRun()) {
			avgSeries.getData().add( new XYChart.Data( i++, a ) );
		}
		Stage stage1 = new Stage( StageStyle.DECORATED );
		final LineChart<Number, Number> lineChart = showGraph( stage1, selectionAlgoName, survivalAlgoName, "", avgSeries, series );

		takeScreenshot( selectionAlgoName, survivalAlgoName, lineChart );
	}

	private void takeScreenshot (final String selectionAlgoName, final String survivalAlgoName, final LineChart<Number, Number> lineChart) {
		Timeline fiveSecondsWonder = new Timeline( new KeyFrame( Duration
				.seconds( 2 ), new EventHandler<ActionEvent>() {

			@Override
			public void handle (ActionEvent event) {
				try {
					SnapshotParameters parameters = new SnapshotParameters();
					WritableImage wi = new WritableImage( 800, 600 );
					WritableImage snapshot = lineChart.snapshot( new SnapshotParameters(), wi );

					File output = new File( "func1_" + selectionAlgoName + survivalAlgoName + ".png" );
					ImageIO.write( SwingFXUtils.fromFXImage( snapshot, null ), "png", output );
				}
				catch (IOException ex) {
					Logger.getLogger( Function1.class.getName() ).log( Level.SEVERE, null, ex );
				}
			}
		} ) );
		fiveSecondsWonder.setCycleCount( 1 );
		fiveSecondsWonder.play();
	}

	private LineChart<Number, Number> showGraph (Stage stage, String selectionAlgoName, String survivalAlgoName, String axisLabel,
												 XYChart.Series... series) {

		stage.setTitle( "Evolutionary Algorithm" );
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		xAxis.setLabel( "Iteration" );
		yAxis.setLabel( axisLabel );

		final LineChart<Number, Number> lineChart =
				new LineChart<Number, Number>( xAxis, yAxis );

		lineChart.setTitle( "Function 1: " + selectionAlgoName + " w/ " + survivalAlgoName );

		Scene scene = new Scene( lineChart, 800, 600 );
		lineChart.getData().addAll( series );

		stage.setScene( scene );
		stage.show();
		return lineChart;
	}
}
