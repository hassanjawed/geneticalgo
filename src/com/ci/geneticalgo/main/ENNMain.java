package com.ci.geneticalgo.main;

import com.ci.geneticalgo.*;
import com.ci.geneticalgo.functions.StockMarketFunction;
import com.ci.geneticalgo.neural_network.ArtificialNeuralNetwork;
import com.ci.geneticalgo.neural_network.Data;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.*;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.apache.commons.csv.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

/**
 * Created by Trikster on 11/1/2015.
 */
public class ENNMain extends Application {

	public static final int                           POPULATION_SIZE     = 50;
	public static final int                           HIDDEN_NEURON_COUNT = 2;
	public static final int                           GENERATIONS         = 2000;
	public static final int                           CONVERGENCE_LIMIT   = 500;
	public static final StockMarketFunction.Selection SELECTION_MODE      = StockMarketFunction.Selection.LINEAR;
	private List<Data> csvData;
	private List<Data> trainData;

	// TODO new run config with terminate condition do MinRBS/MinTruncation on training SMF feed optimized set of weights
// and testing data to neural network and predict

// TODO change neural network to work on training and testing introduce epochs termination and SSE termination
// iteration

	public static void main (String[] args) {
		launch( args );
	}

	@Override
	public void start (Stage primaryStage) throws Exception {
		System.setProperty( "java.util.Arrays.useLegacyMergeSort", "true" );
		runENN( primaryStage );
	}

	private void runENN (Stage stage) throws IOException {
		readData();
		GeneticWorldOld geneticWorld = initWorld();

		long startTime = System.nanoTime();
		ParentSelection minRBS = new MinRankBasedSelection();
		SurvivalSelection minTruncation = new MinTruncation();
		ENNRunConfig runConfig = new ENNRunConfig( GENERATIONS, POPULATION_SIZE, geneticWorld, minRBS, minTruncation );
		ENNRunConfig.setConvergenceLimit( CONVERGENCE_LIMIT );
		runConfig.run();
		StockMarketFunction bestSMF = (StockMarketFunction) runConfig.getBestSoFar();

		List<Double> averageBestSoFar = runConfig.getAverageBestSoFar();
		Map<Integer, List<Double>> avgBSFMap = new HashMap<>();
		avgBSFMap.put( 1, averageBestSoFar );
		Map<Integer, String> avgBSFNameMap = new HashMap<>();
		avgBSFNameMap.put( 1, "MSE" );
		XYChart.Series[] avgBSFSeries = makeSeries( avgBSFMap, avgBSFNameMap );
		Stage stage1 = new Stage( StageStyle.DECORATED );
		LineChart<Number, Number> sseChart = showGraph( stage1, "Weights evolution", "MSE", avgBSFSeries );

		ArtificialNeuralNetwork ann = new ArtificialNeuralNetwork();
		ann.setState( ArtificialNeuralNetwork.State.PREDICTING );
		ann.setWeights( bestSMF.getWeights(), HIDDEN_NEURON_COUNT, 3 );
//		List<Data> results = ann.startWith( csvData, HIDDEN_NEURON_COUNT );
		List<Data> results = ann.test( csvData );
		long endTime = System.nanoTime();
		long elapsedTime = (endTime - startTime) / (1000 * 1000);
		System.out.println( "----------------------------------------------" );
		System.out.println( "Time taken: " + elapsedTime + "secs" );

		Map<Integer, List<Float>> map = new HashMap<>();
		Map<Integer, String> namesMap = new HashMap<>();
		namesMap.put( 1, "Actual" );
		namesMap.put( 2, "Predicted" );

		for (int i = 0; i < results.size(); i++) {
			List<Float> yValues = map.get( 1 );
			if (yValues == null) yValues = new ArrayList<>();
			Data data = results.get( i );
			yValues.add( data.y );
			map.put( 1, yValues );


			List<Float> predictedYValues = map.get( 2 );
			if (predictedYValues == null) predictedYValues = new ArrayList<>();
			predictedYValues.add( data.predictedY() );
			map.put( 2, predictedYValues );
			System.out.println( "Actual: " + data.y + " Prediction: " + data.predictedY() + " " +
					"denormP:" + data.normPredictedY );
		}
		XYChart.Series[] series = makeSeries( map, namesMap );
		LineChart<Number, Number> predictionChart = showGraph( stage, "Evolutionary Neural Network", "Y", series );

		takeScreenshot( "enn_sse_" + getImageNameParams(), sseChart );
		takeScreenshot( "enn_pred_" + getImageNameParams(), predictionChart );
	}

	private String getImageNameParams () {
		StringBuilder sb = new StringBuilder();
		sb.append( "hnc" ).append( HIDDEN_NEURON_COUNT ).append( "_" );
		sb.append( "ps" ).append( POPULATION_SIZE ).append( "_" );
		sb.append( "g" ).append( GENERATIONS ).append( "_" );
		sb.append( "cl" ).append( CONVERGENCE_LIMIT ).append( "_" );
		sb.append( "s" ).append( SELECTION_MODE ).append( "_" );
		return sb.toString();
	}

	private <T> XYChart.Series[] makeSeries (Map<Integer, List<T>> values, Map<Integer, String> namesMap) {
		List<XYChart.Series> seriesList = new ArrayList<>();
		for (int i = 0; i < values.size(); i++) {
			XYChart.Series<Number, T> series = new XYChart.Series<>();
			List<T> dataPoints = values.get( i + 1 );
			series.setName( namesMap.get( i + 1 ) );
			for (int j = 0; j < dataPoints.size(); j++) {
				series.getData().add( new XYChart.Data<>( j, dataPoints.get( j ) ) );
			}
			seriesList.add( series );
		}
		XYChart.Series[] seriesArray = new XYChart.Series[seriesList.size()];
		seriesList.toArray( seriesArray );
		return seriesArray;
	}

	private GeneticWorldOld initWorld () {
		trainData = csvData.subList( 0, (int) (csvData.size() * 0.67f) );
		GeneticWorldOld geneticWorld;
		StockMarketFunction.setData( trainData, HIDDEN_NEURON_COUNT );
		StockMarketFunction.setSelection( SELECTION_MODE );
		geneticWorld = new GeneticWorldOld( POPULATION_SIZE );
		for (int i = 0; i < POPULATION_SIZE; i++) {
			geneticWorld.add( StockMarketFunction.newObject() );
		}
		return geneticWorld;
	}

	private void readData () throws IOException {
		File csvFile = new File( "D:\\My Data\\MS\\CI\\GeneticAlgo\\src\\com\\ci\\geneticalgo\\data.csv" );
		CSVParser parser = CSVParser.parse( csvFile, Charset.defaultCharset(), CSVFormat.EXCEL );

		csvData = new ArrayList<>();
		float max = 0, min = 0;
		for (CSVRecord record : parser) {
			if (parser.getRecordNumber() == 1) continue;
			Data data = new Data();

			for (int i = 0; i < 4; i++) {
				//				System.out.print( record.get( i ) + " | " );
				float v = data.setValue( i, record.get( i ) );
				if (v > max) {
					max = v;
				}
				if (v < min || min == 0) {
					min = v;
				}
			}
			csvData.add( data );
		}
		float diff = max - min;
		for (Data data : csvData) {
			data.normalize( min, diff );
//			data.sigmoidNormalize();
		}
	}

	private void takeScreenshot (final String fileName, final LineChart<Number, Number> lineChart) {
		if (!captureScreenshots()) return;

		Timeline fiveSecondsWonder = new Timeline( new KeyFrame( Duration
				.seconds( 2 ), new EventHandler<ActionEvent>() {

			@Override
			public void handle (ActionEvent event) {
				try {
					SnapshotParameters parameters = new SnapshotParameters();
					WritableImage wi = new WritableImage( 800, 600 );
					WritableImage snapshot = lineChart.snapshot( new SnapshotParameters(), wi );

					File output = new File( fileName + ".png" );
					ImageIO.write( SwingFXUtils.fromFXImage( snapshot, null ), "png", output );
				}
				catch (IOException ex) {
					Logger.getLogger( Function1.class.getName() ).log( Level.SEVERE, null, ex );
				}
			}
		} ) );
		fiveSecondsWonder.setCycleCount( 1 );
		fiveSecondsWonder.play();
	}

	protected boolean captureScreenshots () {
		return true;
	}

	private LineChart<Number, Number> showGraph (Stage stage, String graphName, String axisLabel,
												 XYChart.Series... series) throws MalformedURLException {
		stage.setTitle( "Artificial Neural Network" );
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		xAxis.setLabel( "x" );
		yAxis.setLabel( axisLabel );

		final LineChart<Number, Number> lineChart =
				new LineChart<>( xAxis, yAxis );

		lineChart.setTitle( graphName );

		Scene scene = new Scene( lineChart, 800, 600 );
		File f = new File( "D:\\My Data\\MS\\CI\\GeneticAlgo\\css\\chart.css" );
		URL url = f.toURI().toURL();
		scene.getStylesheets().clear();
		scene.getStylesheets().add( url.toExternalForm() );
		//		scene.getStylesheets().add( "file:///" + f.getAbsolutePath().replace( "\\", "/" ) );
		lineChart.getData().addAll( series );


		stage.setScene( scene );
		stage.show();
		return lineChart;
	}
}
