package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 9/7/2015.
 */
public class BinaryTournament extends EASelector implements SurvivalSelection {

	@Override
	public void selectParentsFrom (List<Member> members) {
		int index1 = (int) Utils.randomFloatBetween( 0, members.size() - 1 );
		int index2 = (int) Utils.randomFloatBetween( 0, members.size() - 1 );
		Member member1 = members.get( index1 );
		Member member2 = members.get( index2 );
		parent1 = member1.getFitness() >= member2.getFitness() ? member1 : member2;

		int index3 = (int) Utils.randomFloatBetween( 0, members.size() - 1 );
		int index4 = (int) Utils.randomFloatBetween( 0, members.size() - 1 );
		Member member3 = members.get( index3 );
		Member member4 = members.get( index4 );
		parent2 = member3.getFitness() >= member4.getFitness() ? member3 : member4;
	}

	@Override
	public List<Member> postSelection (List<Member> population, List<Member> offsprings, int populationSize) {
		population.addAll( offsprings );

		List<Member> newPopulation = new ArrayList<Member>( population );
		while (newPopulation.size() > populationSize) {
			int index1 = (int) Utils.randomFloatBetween( 0, newPopulation.size() - 1 );
			int index2 = (int) Utils.randomFloatBetween( 0, newPopulation.size() - 1 );
			Member member1 = newPopulation.get( index1 );
			Member member2 = newPopulation.get( index2 );
			int indexToRemove = member1.getFitness() > member2.getFitness() ? index2 : index1;
			newPopulation.remove( indexToRemove );
//			newPopulation.add( newPopulation.get( indexToRemove ) );
		}
		return newPopulation;
	}
}
