package com.ci.geneticalgo;

import com.ci.geneticalgo.algos.TicTacBotsWorld;
import com.ci.geneticalgo.algos.TicTacTournament;
import com.ci.geneticalgo.executors.*;
import com.ci.geneticalgo.functions.Member;
import com.ci.geneticalgo.functions.TicTacBot;
import com.ci.geneticalgo.main.Function1;
import com.ci.geneticalgo.neural_network.NeuralNet;
import com.ci.geneticalgo.tictactoe.TicTacController;
import com.ci.geneticalgo.tictactoe.TicTacGame;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.*;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

/**
 * Created by Trikster on 11/23/2015.
 */
public class TicTacToeLearner extends javafx.application.Application implements TicTacExecutor.OnFitBotFoundListener,
		Executor.OnOptimizedIndividualFoundListener<Member> {

	public static final int NEURON_COUNT = 8;
	int currentIteration = 0;
	private TicTacBot fitBot;
	private TicTacBot fitTrainedBot;
	private TicTacController controller;


	public static void main (String[] args) {
		launch(args);
	}

	@Override public void start(Stage primaryStage) throws Exception {

		trainTicTac(primaryStage);
	}

	private void trainTicTac(Stage primaryStage) throws IOException {
		NeuralNet neuralNet = new NeuralNet(9, NEURON_COUNT, 9);
		neuralNet.buildNet();

		TicTacBot.setNeuronCount(9, NEURON_COUNT, 9);
		List<Member> members = new ArrayList<>();
		members.add(new TicTacBot());
		TicTacBotsWorld geneticWorld = new TicTacBotsWorld( members, 10 );

		TicTacTournament selector = new TicTacTournament();
		controller = new TicTacController(neuralNet);
		TicTacTournament.setController(controller);
		TicTacExecutor.GAConfiguration gaConfiguration =
				new TicTacExecutor.GAConfiguration( 10, 100, selector, selector );
		TicTacExecutor executor = new TicTacExecutor(geneticWorld, gaConfiguration);
		executor.setBotFoundListener(this);
		executor.setOptimizedMemberListener(this);
		ResultStore resultStore = executor.execute();


		List<XYChart.Series<Number, Float>> series = makeXYChart(resultStore);
		XYChart.Series[] seriesArray = new XYChart.Series[series.size()];
		series.toArray( seriesArray );
		Stage stage = new Stage( StageStyle.DECORATED );
		LineChart<Number, Number> lineChart = showGraph( stage, "Bot evolution", "Fitness", seriesArray );

		TicTacBot winnerBot = fitBot.getFitness() > fitTrainedBot.getFitness() ? fitBot : fitTrainedBot;
		for (int i = 0; i < winnerBot.getWeights().size(); i++) {
			Float weight = winnerBot.getWeights().get(i);
			float distance = Math.abs(weight);
			if (distance < 0.01f) {
				winnerBot.getWeights().set(i, 0f);
			}
		}
		System.out.println(winnerBot.log()+"\n-------------------------------------------");
		int p1WinCount = 0, p2WinCount = 0, noResult = 0;
		for (int i = 0; i < 5; i++) {
			TicTacGame ticTacGame = controller.evaluateBot( winnerBot );
			if (ticTacGame.getResult() == TicTacGame.Result.P1_WON) {
				p1WinCount += 1;
			} else if (ticTacGame.getResult() == TicTacGame.Result.P2_WON) {
				p2WinCount += 1;
			} else {
				noResult += 1;
			}
			BufferedReader br = new BufferedReader( new InputStreamReader( System.in ) );
			System.out.print( "Bot vs random: Win:"
					+ p1WinCount
					+ " Lost:"
					+ p2WinCount
					+ " NR:"
					+ noResult
					+ "; Enter y to play again:" );
		}
		System.out.println(
				"You are playing with winner of " + (winnerBot.getFitness() - 50) + " games" );
		p1WinCount = 0;
		p2WinCount = 0;
		noResult = 0;
		while (true) {
			TicTacGame ticTacGame = controller.versusHuman(winnerBot);
			if (ticTacGame.getResult() == TicTacGame.Result.P1_WON) {
				p1WinCount += 1;
			} else if (ticTacGame.getResult() == TicTacGame.Result.P2_WON) {
				p2WinCount += 1;
			} else {
				noResult += 1;
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Bot results: Win:"
					+ p1WinCount
					+ " Lost:"
					+ p2WinCount
					+ " NR:"
					+ noResult );
			if (String.format("%c", br.read()).equalsIgnoreCase("y")) {
				continue;
			} else if (String.format("%c", br.read()).equalsIgnoreCase("q")) {
				System.exit(0);
			} else {
				break;
			}
		}
		takeScreenshot( "tt_evolution", lineChart );
	}

	private void takeScreenshot (final String fileName, final LineChart<Number, Number> lineChart) {
		Timeline fiveSecondsWonder = new Timeline( new KeyFrame( Duration
				.seconds( 2 ), new EventHandler<ActionEvent>() {

			@Override
			public void handle (ActionEvent event) {
				try {
					SnapshotParameters parameters = new SnapshotParameters();
					WritableImage wi = new WritableImage( 800, 600 );
					WritableImage snapshot = lineChart.snapshot( new SnapshotParameters(), wi );

					File output = new File( fileName + ".png" );
					ImageIO.write( SwingFXUtils.fromFXImage( snapshot, null ), "png", output );
				}
				catch (IOException ex) {
					Logger.getLogger( Function1.class.getName() ).log( Level.SEVERE, null, ex );
				}
			}
		} ) );
		fiveSecondsWonder.setCycleCount( 1 );
		fiveSecondsWonder.play();
	}

	@Override public void onBotFound(int currentIteration, TicTacBot newBot) {
		if (this.fitBot == null) {
			fitBot = newBot;
		} else if (fitBot.getFitness() < newBot.getFitness()) {
			fitBot = newBot;
		}
	}

	@Override public void optimizedMember(Member member) {
		TicTacBot newBot = (TicTacBot) member;
		if (this.fitTrainedBot == null) {
			fitTrainedBot = newBot;
		} else if (fitTrainedBot.getFitness() < newBot.getFitness()) {
			fitTrainedBot = newBot;
		}
	}

	private List<XYChart.Series<Number, Float>> makeXYChart (ResultStore resultStore) {
		XYChart.Series<Number, Float> avgBSFSeries = new XYChart.Series<>();
		avgBSFSeries.setName( "Avg BSF" );
		XYChart.Series<Number, Float> avgFitnessSeries = new XYChart.Series<>();
		avgFitnessSeries.setName( "Avg Fitness" );
		List<Float> fitness = resultStore.calculateAverageAverageFitness();
		List<Float> bsf = resultStore.calculateAverageBestSoFar();
		for (int i = 0; i < fitness.size(); i++) {
			avgBSFSeries.getData().add( new XYChart.Data<>( i + 1, bsf.get( i ) ) );
			avgFitnessSeries.getData().add( new XYChart.Data<>( i + 1, fitness.get( i ) ) );
		}
		List<XYChart.Series<Number, Float>> allSeries = new ArrayList<>();
		allSeries.add( avgBSFSeries );
		allSeries.add( avgFitnessSeries );
		return allSeries;
	}

	private LineChart<Number, Number> showGraph (Stage stage, String graphName, String axisLabel,
												 XYChart.Series... series) {

		stage.setTitle( "Tic Tac Toe" );
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		xAxis.setLabel( "# of generation" );
		yAxis.setLabel( axisLabel );

		final LineChart<Number, Number> lineChart =
				new LineChart<Number, Number>( xAxis, yAxis );

		lineChart.setTitle( graphName );

		Scene scene = new Scene( lineChart, 800, 600 );
		lineChart.getData().addAll( series );

		stage.setScene( scene );
		stage.show();
		return lineChart;
	}
}
