package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.*;

/**
 * Created by Trikster on 11/1/2015.
 */
public class MinRankBasedSelection extends EASelector {

	@Override
	public void selectParentsFrom (List<Member> members) {
		List<Member> sortedMembers = new ArrayList<>( members );
		Collections.sort( sortedMembers );

		int sum = 0;
		for (int i = 1; i <= sortedMembers.size(); i++) sum += i;

		float lastP = 0;
		float p;

		List<ProbRange> ranges = new ArrayList<>( sortedMembers.size() );
		for (int i = 0; i < sortedMembers.size(); i++) {
			p = (float) (sortedMembers.size() - i) / sum;
			ranges.add( new ProbRange( lastP, lastP + p ) );
			lastP += p;
		}
		do {
			parent1 = selectParent( sortedMembers, ranges, Math.random() );
		} while (parent1 == null);

		do {
			parent2 = selectParent( sortedMembers, ranges, Math.random() );
		} while (parent2 == null);
	}

	private Member selectParent (List<Member> members, List<ProbRange> maxProb, double random) {
		for (int i = 0; i < members.size(); i++) {
			ProbRange thisProb = maxProb.get( i );
			if (random > thisProb.min && random <= thisProb.max) {
				return members.get( i );
			}
		}
		return null;
	}


	private static class ProbRange {

		float min;
		float max;

		public ProbRange (float min, float max) {
			this.min = min;
			this.max = max;
		}
	}
}
