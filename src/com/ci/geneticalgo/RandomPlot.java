package com.ci.geneticalgo;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.stage.Stage;

/**
 * Created by Trikster on 9/6/2015.
 */
public class RandomPlot extends Application {

	private static double[] x;
	private static double[] y;
	private static final int SIZE = 5000;

	public static void main (String[] args) {
		x = new double[SIZE];
		y = new double[SIZE];

		for (int i = 0; i < SIZE; i++) {
			x[i] = Utils.randomFloatBetween( -5, 5 );
		}
		for (int i = 0; i < SIZE; i++) {
			y[i] = Utils.randomFloatBetween( -5, 5 );
		}
		launch( args );
	}

	@Override
	public void start (Stage stage) throws Exception {
		stage.setTitle("Random Plot");
		final NumberAxis xAxis = new NumberAxis(-6, 6, 1);
		final NumberAxis yAxis = new NumberAxis(-6, 6, 1);
		final ScatterChart<Number,Number> sc = new
				ScatterChart<Number,Number>(xAxis,yAxis);
		xAxis.setLabel("x");
		yAxis.setLabel("y");
		sc.setTitle("Random Points");

		XYChart.Series series1 = new XYChart.Series();
		series1.setName("Random Points");
		for (int i = 0; i < x.length; i++) {
			series1.getData().add( new XYChart.Data( x[i], y[i] ) );
		}

		sc.getData().addAll(series1);
		Scene scene  = new Scene(sc, 500, 400);
		stage.setScene(scene);
		stage.show();
	}
}
