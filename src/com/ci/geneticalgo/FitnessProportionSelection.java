package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.*;

/**
 * Created by Trikster on 9/6/2015.
 */
public class FitnessProportionSelection extends EASelector implements ParentSelection, SurvivalSelection {



	@Override
	public void selectParentsFrom (List<Member> members) {
		double sum = 0;
		List<ProbRange> maxProb = new ArrayList<ProbRange>( members.size() );
		for (Member member : members) sum += member.getFitness();

		float tempProb = 0, temp;

		for (Member member : members) {
			maxProb.add( new ProbRange( tempProb, temp = (float) (member.getFitness() / sum) ) );
			tempProb = temp;
		}

		do {
			double random1 = Math.random();
			parent1 = selectParent( members, maxProb, random1 );
		} while (parent1 == null);

		do {
			double random2 = Math.random();
			parent2 = selectParent( members, maxProb, random2 );
		} while (parent2 == null);
	}

	private Member selectParent (List<Member> members, List<ProbRange> maxProb, double random) {
		for (int i = 0; i < members.size(); i++) {
			ProbRange thisProb = maxProb.get( i );
			if (random > thisProb.min && random <= thisProb.max) {
				return members.get( i );
			}
		}
		return null;
	}

	@Override
	public List<Member> postSelection (List<Member> population, List<Member> offsprings, int populationSize) {
		List < Member > survivedMembers = new ArrayList<Member>();
		if (population.size() + offsprings.size() <= populationSize) {
			survivedMembers.addAll( population );
			survivedMembers.addAll( offsprings );
			return survivedMembers;
		}

		List<Member> wholePopulation = new ArrayList<Member>( population );
		wholePopulation.addAll( offsprings );
		double sum = 0;
		List<ProbRange> maxProb = new ArrayList<ProbRange>( wholePopulation.size() );
		for (Member member : wholePopulation) {
			member.calculateFitness();
			sum += member.getFitness();
		}

		float tempProb = 0, temp;

		for (Member member : wholePopulation) {
			maxProb.add( new ProbRange( tempProb, temp = (float) (member.getFitness() / sum) ) );
			tempProb = temp;
		}

		do {
			double random1 = Math.random();
			Member survivedMember = selectParent( wholePopulation, maxProb, random1 );
			if (survivedMember != null) {
				survivedMembers.add( survivedMember );
			}
		} while (survivedMembers.size() != populationSize);
		return survivedMembers;
	}

	private static class ProbRange {

		float min;
		float max;

		public ProbRange (float min, float max) {
			this.min = min;
			this.max = max;
		}
	}
}
