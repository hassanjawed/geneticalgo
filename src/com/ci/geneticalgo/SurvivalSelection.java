package com.ci.geneticalgo;


import com.ci.geneticalgo.functions.Member;

import java.util.List;

/**
 * Created by Trikster on 9/6/2015.
 */
public interface SurvivalSelection {

	List<Member> postSelection (List<Member> population, List<Member> offsprings, int populationSize);
}
