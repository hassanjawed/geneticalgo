package com.ci.geneticalgo.neural_network;

import com.ci.geneticalgo.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 11/24/2015.
 */
public class NeuralNet {

	public static final float LEARNING_RATE = 0.6f;

	private final int inputFeatures;
	private final int neuronCount;
	private final int outputClasses;

	List<Float>  inputNeurons;
	List<Neuron> hiddenNeurons;
	List<Neuron> outputNeurons;

	public NeuralNet (int inputFeatures, int neuronCount, int outputClasses) {
		inputNeurons = new ArrayList<>(this.inputFeatures = inputFeatures);
		hiddenNeurons = new ArrayList<>(this.neuronCount = neuronCount);
		outputNeurons = new ArrayList<>(this.outputClasses = outputClasses);
	}

	public void buildNet() {
		for (int i = 0; i < inputFeatures; i++) inputNeurons.add(0f);

		for (int i = 0; i < neuronCount; i++) hiddenNeurons.add(new Neuron());

		for (int i = 0; i < outputClasses; i++) outputNeurons.add(new Neuron());
	}

	public List<Float> produceOutput (List<Float> inputs, List<Float> weights) {
		if (inputNeurons.size() != inputs.size()) {
			throw new IllegalArgumentException( "Inputs count must be " + inputNeurons.size() );
		}

		for (int i = 0; i < inputs.size(); i++) {
			inputNeurons.set( i, Utils.hyperbolicTangent( inputs.get( i ) ) );
		}

		int weightIndex = 0;
		List<Float> hiddenOutputs = new ArrayList<>();

		for (Neuron neuron : hiddenNeurons) {
			neuron.setWeights( weights.subList( weightIndex, weightIndex += inputs.size() ) );
			hiddenOutputs.add( neuron.calculateOutput( inputNeurons ) );
		}

		List<Float> outputs = new ArrayList<>();
		for (Neuron neuron : outputNeurons) {
			neuron.setWeights( weights.subList( weightIndex, weightIndex += neuronCount ) );
			outputs.add( neuron.calculateOutput( hiddenOutputs ) );
		}
		return outputs;
	}

	private class Neuron {

		private List<Float> weights;
		private float       output;

		public void randInitWeights (int size, float min, float max) {
			weights = new ArrayList<>( size );
			for (int i = 0; i < size; i++) {
				weights.add( Utils.randomFloatBetween( min, max ) );
			}
		}

		public float calculateOutput (List<Float> inputs) {
			double sum = 0;
			for (int i = 0; i < inputs.size(); i++) {
				sum = sum + (inputs.get( i ) * weights.get( i ));
			}
			return output = Utils.sigmoid( sum );
		}

		public void setWeights (List<Float> inputWeights) {
			this.weights = new ArrayList<>( inputWeights );
		}
	}
}
