package com.ci.geneticalgo.neural_network;


import com.ci.geneticalgo.Utils;

import java.util.*;

/**
 * Created by Trikster on 10/24/2015.
 */
public class ArtificialNeuralNetwork {

	public static final int   MIN_WEIGHT     = -2;
	public static final int   MAX_WEIGHT     = 2;
	public static final float LEARNING_RATE  = 0.6f;
	public static final float SSE_THRESHOLD  = 1000f;
	public static final float STOP_THRESHOLD = 100f;
	public static final int   INPUT_COUNT    = 3;

	private List<Float>        inputs;
	private List<Neuron>       hiddenNeurons;
	private List<OutputNeuron> outputNeurons;

	private int runCount   = 1;
	private int inputIndex = 0;

	private List<Data> results;
	private float lastSSE = 0;
	private State state   = State.LEARNING;

	public enum State {LEARNING, PREDICTING, NETWORK_READY}

	public ArtificialNeuralNetwork () {
		hiddenNeurons = new ArrayList<>();
	}

	public void setState (State state) {
		this.state = state;
	}

	public void train (List<Data> trainData, int epochs, int hiddenNeuronCount) {
		if (hiddenNeurons.size() != hiddenNeuronCount) {
			throw new IllegalStateException( "Hidden neurons are in illegal state count:" + hiddenNeuronCount +
					" init:" + hiddenNeurons.size() );
		}
		List<Data> trainingData = new ArrayList<>( trainData );
		for (int i = 0; i < epochs; i++) {
			Collections.shuffle( trainingData );
			for (Data data : trainingData) {
				inputs = Arrays.asList( data.normInputs );

				outputNeurons = new ArrayList<>();
				outputNeurons.add( new OutputNeuron( data.normY ) );

				float output = calculateOutput();
				float sse = data.getSSE( output );

				// Termination on two consecutive SSE
				if (lastSSE != 0 && Math.abs( sse - lastSSE ) < STOP_THRESHOLD) {
					setState( State.NETWORK_READY );
					return;
				}

				// Weight condition on SSE threshold
				if (sse > SSE_THRESHOLD) {
					adjustWeights( output );
				}
				output = calculateOutput();
				lastSSE = data.getSSE( output );
			}
		}
		setState( State.NETWORK_READY );
	}

	public List<Data> test (List<Data> testData) {
		if (state != State.NETWORK_READY) {
			throw new IllegalStateException( "Network is not ready" );
		}
		setState( State.PREDICTING );
		results = new ArrayList<>();
		for (Data data : testData) {
			inputs = Arrays.asList( data.normInputs );
			data.normPredictedY = calculateOutput();
			results.add( data );
		}

		return results;
	}

	public List<Data> startWith (List<Data> data, int hiddenNeuronCount) {
		if (hiddenNeurons.size() != hiddenNeuronCount) {
			throw new IllegalStateException( "Hidden neurons are in illegal" +
					" state" );
		}
		for (Data d : data) {
			inputIndex++;
			inputs = Arrays.asList( d.normInputs );
			outputNeurons = new ArrayList<>();
			outputNeurons.add( new OutputNeuron( d.normY ) );

			float output = calculateOutput();
			if (state == State.PREDICTING) {
				d.normPredictedY = output;
				continue;
			}
			float sse = d.getSSE( output );
			if (lastSSE != 0) {
				if (Math.abs( sse - lastSSE ) < STOP_THRESHOLD) {
					System.out.print( "Predicting...." );
					state = State.PREDICTING;
					continue;
				} else {
					lastSSE = sse;
				}
			}
			System.out.print( "Minimizing SSE...." + sse );
			while (sse > SSE_THRESHOLD) {
				adjustWeights( output );
				output = calculateOutput();
				sse = d.getSSE( output );
			}
			System.out.println( " Done...." + sse );
			d.normPredictedY = output;
		}
		return data;
	}

	public void initHiddenLayer (int hiddenNeuronCount) {
		Neuron temp;
		for (int i = 0; i < hiddenNeuronCount; i++) {
			temp = new Neuron();
			temp.randInitWeights( INPUT_COUNT, MIN_WEIGHT, MAX_WEIGHT );
			this.hiddenNeurons.add( temp );
		}
	}

	public void setWeights (List<Float> weights, int neuronCount, int inputCount) {
		int weightsRequired = neuronCount * (1 + inputCount);
		if (weights.size() < weightsRequired) {
			throw new IllegalArgumentException( "Insufficient weights require " + weightsRequired );
		}
		hiddenNeurons = new ArrayList<>();
		int startIndex = 0, endIndex = startIndex + inputCount;
		for (int i = 0; i < neuronCount; i++) {
			Neuron neuron = new Neuron();
			neuron.setWeights( weights.subList( startIndex, endIndex ) );
			hiddenNeurons.add( neuron );

			startIndex += inputCount;
			endIndex += inputCount;
		}
//		weights.removeAll( weights.subList( 0, startIndex ) );
		for (int i = 0; i < hiddenNeurons.size(); i++) {
			hiddenNeurons.get( i ).outputWeight = weights.get( startIndex + i );
		}
		setState( State.NETWORK_READY );
	}

	public void adjustWeights (float output) {
		for (OutputNeuron outputNeuron : outputNeurons) {
			outputNeuron.calculateError( output );
			adjustWeights( outputNeuron );
		}
	}

	private float calculateOutput () {
		float output = 0;
		for (Neuron hiddenNeuron : hiddenNeurons) {
			List<Float> newInputs = new ArrayList<>( inputs );
			output += (hiddenNeuron.calculateOutput( newInputs ) * hiddenNeuron.outputWeight);
		}
		output = Utils.sigmoid( output );
		return output;
	}

	public void adjustWeights (OutputNeuron outputNeuron) {
		for (Neuron neuron : hiddenNeurons) {
			neuron.adjustWeight( inputs, outputNeuron.error );
		}
	}

	private class Neuron {

		private List<Float> weights;
		private float       output;
		private float       outputWeight;

		public void randInitWeights (int size, float min, float max) {
			weights = new ArrayList<>( size );
			for (int i = 0; i < size; i++) {
				weights.add( Utils.randomFloatBetween( min, max ) );
			}
			outputWeight = Utils.randomFloatBetween( min, max );
		}

		public float calculateOutput (List<Float> inputs) {
			double sum = 0;
			for (int i = 0; i < inputs.size(); i++) {
				sum = sum + (inputs.get( i ) * weights.get( i ));
			}
			return output = Utils.sigmoid( sum );
		}

		public void adjustWeight (List<Float> inputs, float outputError) {
			outputWeight = outputWeight + LEARNING_RATE * outputError * output;
			for (int i = 0; i < weights.size(); i++) {
				Float w = weights.get( i );
				weights.set( i, w + outputError * w * output * (1 - output) * inputs.get( i ) );
			}
		}

		public void setWeights (List<Float> inputWeights) {
			this.weights = new ArrayList<>( inputWeights );
		}
	}

	private class OutputNeuron {

		private float output;
		private float targetOutput;
		private float error;

		public OutputNeuron (float targetOutput) {
			this.targetOutput = targetOutput;
		}

		public void calculateError (float output) {
			this.output = output;
			error = output * (1 - output) * (targetOutput - output);
		}
	}
}
