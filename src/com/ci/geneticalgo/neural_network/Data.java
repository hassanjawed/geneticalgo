package com.ci.geneticalgo.neural_network;

import com.ci.geneticalgo.Utils;
import com.sun.corba.se.impl.javax.rmi.CORBA.Util;

public class Data {

	public static float min;
	public static float diff;

	public float y;
	public float normY;
	public float normPredictedY;

	public Float[] inputs;
	public Float[] normInputs;

	public Data () {
		inputs = new Float[3];
		normInputs = new Float[3];
	}

	public float setValue (int index, String value) {
		if (index == 3) {
			return y = Float.parseFloat( value );
		} else if (index >= 0 && index < 3) {
			return inputs[index] = Float.parseFloat( value );
		} else {
			throw new IllegalArgumentException( "Invalid index " + index );
		}
	}

	/**
	 * normalizedX = (x - min)/(max - min)
	 *
	 * @param min
	 * @param diff max - min
	 */
	public void normalize (float min, float diff) {
		Data.min = min;
		Data.diff = diff;
		normY = (y - min) / diff;
		for (int i = 0; i < inputs.length; i++) {
			normInputs[i] = (inputs[i] - min) / diff;
		}
	}

	public void sigmoidNormalize () {
		normY = Utils.sigmoid( normY );
		for (int i = 0; i < inputs.length; i++) {
			normInputs[i] = Utils.sigmoid( normInputs[i] );
		}
	}

	/**
	 * DenormalizedX = normalizedX * (max - min) + min
	 *
	 * @param normPredictedOutput normalized predicted output
	 * @param min
	 * @param diff    max-min
	 * @return SSE (predictedY - TargetY)^2
	 */
	public float getSSE (float normPredictedOutput) {
		float denormOutput = normPredictedOutput * diff + min;
		return (float) Utils.square( denormOutput - y );
	}

	public float predictedY () {
		return normPredictedY * diff + min;
	}

	public String log () {
		StringBuilder sb = new StringBuilder();
		sb.append( "[" );
		for (float v : inputs) {
			sb.append( v ).append( "," );
		}
		sb.append( "]" );
		sb.append( y );
		sb.append( "\tnormX:[" );
		for (float v : normInputs) {
			sb.append( v ).append( "," );
		}
		sb.append( "]" );
		sb.append( normY );
		return sb.toString();
	}
}
