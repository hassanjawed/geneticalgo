package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.*;

/**
 * Created by Trikster on 11/1/2015.
 */
public class MinTruncation implements SurvivalSelection {

	@Override
	public List<Member> postSelection (List<Member> population, List<Member> offsprings, int populationSize) {
		ArrayList<Member> members = new ArrayList<>( population );
		Collections.sort( members, (o1, o2) -> -o1.compareTo( o2 ) );
		int size = members.size();

		int fitSize = (int) Math.floor( size * 0.4f ); // Taking 30% fittest members
		fitSize = Math.min( fitSize, offsprings.size() );
		List<Member> selectedMembers = members.subList( 0, size - fitSize );

		ArrayList<Member> newPopulation = new ArrayList<>( offsprings );
		Collections.sort( newPopulation, (o1, o2) -> -o1.compareTo( o2 ) );
		selectedMembers.addAll( newPopulation.subList( 0, populationSize - selectedMembers.size() ) );
		return selectedMembers;
	}
}

