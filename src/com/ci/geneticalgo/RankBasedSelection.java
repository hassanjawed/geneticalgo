package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 9/7/2015.
 */
public class RankBasedSelection extends EASelector {


	@Override
	public void selectParentsFrom (List<Member> members) {
		float lastP = 0;
		float p = 1f / members.size();
		List<ProbRange> ranges = new ArrayList<ProbRange>( members.size() );
		for (int i = 0; i < members.size(); i++) {
			ranges.add( new ProbRange( lastP, lastP + p ) );
			lastP += p;
		}
		do {
			parent1 = selectParent( members, ranges, Math.random() );
		} while (parent1 == null);

		do {
			parent2 = selectParent( members, ranges, Math.random() );
		} while (parent2 == null);
	}

	private Member selectParent (List<Member> members, List<ProbRange> maxProb, double random) {
		for (int i = 0; i < members.size(); i++) {
			ProbRange thisProb = maxProb.get( i );
			if (random > thisProb.min && random <= thisProb.max) {
				return members.get( i );
			}
		}
		return null;
	}


	private static class ProbRange {

		float min;
		float max;

		public ProbRange (float min, float max) {
			this.min = min;
			this.max = max;
		}
	}
}
