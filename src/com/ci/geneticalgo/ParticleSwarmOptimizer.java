package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;
import com.ci.geneticalgo.functions.PointParticle;

import java.util.*;

/**
 * Created by Trikster on 10/24/2015.
 */
public class ParticleSwarmOptimizer implements ParentSelection, SurvivalSelection {


	private static final int C1 = 2;
	private static final int C2 = 2;
	double        gBest;
	PointParticle globalBestMember;
	private List<PointParticle> population;

	@Override
	public void selectParentsFrom (List<Member> members) {
		population = new ArrayList<>();
		for (Member member : members) {
			population.add( (PointParticle) member );
		}
		List<PointParticle> sortedMembers = new ArrayList<>( population );
		Collections.sort( sortedMembers );
		globalBestMember = sortedMembers.get( 0 );
		gBest = globalBestMember.getFitness();
	}

	@Override
	public List<Member> getParents () {
		ArrayList<Member> members = new ArrayList<>();
		members.add( globalBestMember );
		return members;
	}

	@Override
	public void performRecombinition () {
		for (PointParticle particle : population) {
			particle.checkPBest();
			particle.updateVelocity( globalBestMember );
		}
	}

	@Override
	public void performMutation () {

	}

	@Override
	public List<Member> getPopulation () {
		List<Member> members = getMembers();
		return members;
	}

	private List<Member> getMembers () {
		List<Member> members = new ArrayList<>();
		for (PointParticle particle : population) {
			members.add( particle );
		}
		return members;
	}

	@Override
	public List<Member> getOffsprings () {
		return getMembers();
	}

	@Override
	public List<Member> postSelection (List<Member> population, List<Member> offsprings, int populationSize) {
		return population;
	}
}
