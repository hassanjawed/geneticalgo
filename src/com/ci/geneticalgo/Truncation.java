package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.*;

/**
 * Created by Trikster on 9/6/2015.
 */
public class Truncation implements SurvivalSelection {

	@Override
	public List<Member> postSelection (List<Member> population, List<Member> offsprings, int populationSize) {
		ArrayList<Member> members = new ArrayList<Member>( population );
		Collections.sort( members );
		int size = members.size();

//		if (offsprings.size() > size * 0.5) {
//			throw new IllegalArgumentException( "Too many offspring to accommodate in the population" );
//		}

		int fitSize = (int) Math.floor( size * 0.4f ); // Taking 30% fittest members

		List<Member> selectedMembers = members.subList( 0, size - fitSize );

		ArrayList<Member> newPopulation = new ArrayList<Member>( offsprings );
		Collections.sort( newPopulation );
		selectedMembers.addAll( newPopulation.subList( 0, populationSize - selectedMembers.size() ) );
//		if (selectedMembers.size() >= populationSize) {
//			return selectedMembers.subList( 0, populationSize );
//		} else {
//			for (int i = selectedMembers.size() - 1; i < populationSize; i++) {
//				selectedMembers.add( selectedMembers.get( i % selectedMembers.size() ) );
//			}
//		}
		return selectedMembers;
	}
}
