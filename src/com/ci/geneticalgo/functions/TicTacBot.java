package com.ci.geneticalgo.functions;

import com.ci.geneticalgo.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 11/23/2015.
 */
public class TicTacBot implements Member {

	public static final float WEIGHT_RANGE   = 2f;
	public static final float MUTATION_RANGE = 0.05f;
	public static final int   MUTATED_GENES  = -1;

	private static int neuronCount;
	private static int weightsCount;

	List<Float> weights;
	private double fitness;
	private boolean hasPlayed = false;

	public TicTacBot() {
		fitness = 50;
		weights = new ArrayList<>();
	}

	public static void setNeuronCount (int inputCount, int neuronCount, int outputCount) {
		TicTacBot.neuronCount = neuronCount;
		weightsCount = inputCount * neuronCount + outputCount * neuronCount;
	}

	private void initWeights () {
		weights = new ArrayList<>();
		for (int i = 0; i < weightsCount; i++) {
			weights.add( Utils.randomFloatBetween( -WEIGHT_RANGE, WEIGHT_RANGE ) );
		}
	}

	public boolean isHasPlayed () {
		return hasPlayed;
	}

	public void setHasPlayed (boolean hasPlayed) {
		this.hasPlayed = hasPlayed;
	}

	public List<Float> getWeights () {
		return weights;
	}

	@Override
	public Member newMember () {
		TicTacBot ticTacBot = new TicTacBot();
		ticTacBot.initWeights();
		return ticTacBot;
	}

	@Override
	public void calculateFitness () {
		throw new IllegalAccessError( "Bot cant calculate fitness" );
	}

	@Override
	public double getFitness () {
		return fitness;
	}

	public void setFitness (double fitness) {
		this.fitness = fitness;
	}

	@Override
	public List<Member> recombine (Member parent2) {
		TicTacBot firstParent = this;
		TicTacBot secondParent = (TicTacBot) parent2;
		TicTacBot child1 = new TicTacBot();
		TicTacBot child2 = new TicTacBot();

		List<Float> child1Weights = new ArrayList<>( weightsCount );
		List<Float> child2Weights = new ArrayList<>( weightsCount );

		int n = 2;
		int geneCount = weightsCount / (n + 1);
		int min = 1, max = geneCount;
		int breakIndex = 0;

		child1Weights.add( firstParent.weights.get( 0 ) );
		child2Weights.add( secondParent.weights.get( 0 ) );

		for (int i = 0; i < n; i++) {
			breakIndex = Utils.randomIntBetween( min, max );
			for (int j = min; j < breakIndex; j++) {
				child1Weights.add( firstParent.weights.get( j ) );
				child2Weights.add( secondParent.weights.get( j ) );
			}
			TicTacBot temp = firstParent;
			firstParent = secondParent;
			secondParent = temp;
			min = breakIndex;
			max = min + geneCount;
		}

		for (int j = breakIndex; j < weightsCount; j++) {
			child1Weights.add( firstParent.weights.get( j ) );
			child2Weights.add( secondParent.weights.get( j ) );
		}

//		int breakIndex = Utils.randomIntBetween( 1, weightsCount - 1 );
//
//
//		for (int i = 0; i < weightsCount; i++) {
//			if (i < breakIndex) {
//				child1Weights.add( firstParent.weights.get( i ) );
//				child2Weights.add( secondParent.weights.get( i ) );
//			} else {
//				child1Weights.add( secondParent.weights.get( i ) );
//				child2Weights.add( firstParent.weights.get( i ) );
//			}
//		}

		child1.weights = child1Weights;
		child2.weights = child2Weights;

		List<Member> children = new ArrayList<>();
		children.add( child1 );
		children.add( child2 );
		return children;
	}

	@Override
	public Member mutate () {
		TicTacBot bot = new TicTacBot();
		for (int i = 0; i < weightsCount; i++) {
			bot.weights.add( weights.get( i ) );
		}

		if (MUTATED_GENES <= 0) {
			for (int i = 0; i < weightsCount * 0.1f; i++) {
				int index = Utils.randomIntBetween(0, weightsCount - 1);
				bot.weights.set(index,
						bot.weights.get(index) + Utils.randomFloatBetween(-MUTATION_RANGE, MUTATION_RANGE));
			}
		} else {
			for (int i = 0; i < MUTATED_GENES; i++) {
				int index = Utils.randomIntBetween( 0, weightsCount );
				bot.weights.set( index, bot.weights.get( index ) + Utils
						.randomFloatBetween( -MUTATION_RANGE, MUTATION_RANGE ) );
			}
		}
		return bot;
	}

	@Override
	public Member gaussianMutation () {
		return null;
	}

	@Override
	public String log () {
		StringBuilder sb = new StringBuilder( "Fitness:" + fitness + " item:" );
		for (Float weight : weights) {
			sb.append( weight ).append( ", " );
		}
		return sb.toString();
	}

	@Override
	public void checkValues () {

	}

	@Override
	public int compareTo (Member o) {
		return (int) (getFitness() - o.getFitness());
	}
}
