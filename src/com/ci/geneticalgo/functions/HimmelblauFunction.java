package com.ci.geneticalgo.functions;

import com.ci.geneticalgo.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.ci.geneticalgo.Utils.square;

/**
 * Created by Trikster on 10/11/2015.
 */
public class HimmelblauFunction extends PointParticle {

	public static final int GENES_COUNT = 2;

	private HimmelblauFunction (List<Range> ranges) {
		super( GENES_COUNT, ranges );
	}

	public static HimmelblauFunction init () {
		Range range = new Range();
		range.max = 4;
		range.min = -4;

		Range range2 = new Range();
		range2.max = 4;
		range2.min = -4;

		List<Range> ranges = new ArrayList<>();
		ranges.add( range );
		ranges.add( range2 );
		HimmelblauFunction obj = new HimmelblauFunction( ranges );
		for (int i = 0; i < GENES_COUNT; i++) {
			obj.genes.add( Utils.randomFloatBetween( ranges.get( i ).min, ranges.get( i ).max ) );
		}
		return obj;
	}

	@Override
	public void calculateFitness () {
		float x = getGeneAt( 0 );
		float y = getGeneAt( 1 );
		fitness = square( square( x ) + y - 11 ) + square( x + square( y ) - 7 );
	}

	@Override
	public AISMember newRandom () {
		return HimmelblauFunction.init();
	}

	@Override
	protected PointMember newPoint () {
		return HimmelblauFunction.init();
	}
}
