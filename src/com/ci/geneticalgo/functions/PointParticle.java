package com.ci.geneticalgo.functions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 10/24/2015.
 */
public abstract class PointParticle extends PointMember implements Particle {

	public static final int C1 = 2;
	public static final int C2 = 2;

	private List<Float> velocityVector;
	private PointMember pBestMember;

	public PointParticle (int genesCount, List<Range> ranges) {
		super( genesCount, ranges );
		velocityVector = new ArrayList<>();
		for (int i = 0; i < genesCount; i++) {
			velocityVector.add( 0f );
		}
	}

	private PointMember cloneSelf () {
		PointMember pointMember = newPoint();
		for (int i = 0; i < genes.size(); i++) {
			pointMember.genes.add( genes.get( i ) );
		}
		pointMember.fitness = fitness;
		return pointMember;
	}

	@Override
	public void checkPBest () {
		pBestMember = pBestMember == null || getFitness() > pBestMember.getFitness() ? cloneSelf() : pBestMember;
	}

	@Override
	public void updateVelocity (PointParticle gBest) {
		for (int i = 0; i < genesCount; i++) {
			Float present = getGeneAt( i );
			Float velocity = velocityVector.get( i );
			velocity = (float) (velocity + C1 * Math.random() * (pBestMember.getGeneAt( i ) - present)
					+ C2 * Math.random() * (gBest.getGeneAt( i ) - present));
			velocityVector.set( i, velocity );
			setGeneAt( i, present + velocity );
		}
	}
}
