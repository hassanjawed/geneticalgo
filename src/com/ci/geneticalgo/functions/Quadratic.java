package com.ci.geneticalgo.functions;

import com.ci.geneticalgo.Chromosome;
import com.ci.geneticalgo.Utils;

import java.util.*;

/**
 * Created by Trikster on 9/5/2015.
 */
public class Quadratic extends Chromosome implements Member {


	public static final float MIN = -5;
	public static final float MAX = 5;
	float  x;
	float  y;
	double fitness;

	private Quadratic () {
	}

	public static Quadratic initRandom () {
		Quadratic point = new Quadratic();
		point.x = Utils.randomFloatBetween( MIN, MAX );
		point.y = Utils.randomFloatBetween( MIN, MAX );
		point.fitness = 0;
		return point;
	}

	@Override
	public Member newMember () {
		return initRandom();
//		x = Utils.randomFloatBetween( MIN, MAX );
//		y = Utils.randomFloatBetween( MIN, MAX );
//		fitness = 0;
	}

	@Override
	public void calculateFitness () {
		fitness = Utils.square( x ) + Utils.square( y );
	}

	@Override
	public double getFitness () {
		return fitness;
	}

	@Override
	public List<Member> recombine (Member parent2) {
		float x1 = x, x2 = ((Quadratic) parent2).x;
		float y1 = y, y2 = ((Quadratic) parent2).y;

		Quadratic point1 = new Quadratic();
		point1.x = x1;
		point1.y = y2;
		point1.calculateFitness();

		Quadratic point2 = new Quadratic();
		point2.x = x2;
		point2.y = y1;
		point2.calculateFitness();

		List<Member> offsprings = new ArrayList<Member>( 2 );
		offsprings.add( point1 );
		offsprings.add( point2 );
		return offsprings;
	}

	@Override
	public Member mutate () {
		Quadratic point = new Quadratic();
		point.x = x < 0 ? minus( x ) : add( x );
		point.y = y < 0 ? minus( y ) : add( y );
		return point;
	}

	@Override
	public Member gaussianMutation () {
		Quadratic point = new Quadratic();

		Random random = new Random();
		point.x = Utils.gaussianBetweenMinMax( MIN, MAX );
		point.y = Utils.gaussianBetweenMinMax( MIN, MAX );

		return point;
	}

	@Override
	public String log () {
		StringBuilder builder = new StringBuilder();
		builder.append( "x:" ).append( x ).append( " y:" ).append( y ).append( " fit:" ).append( fitness );
		return builder.toString();
	}

	@Override
	public void checkValues () {
		if (x > MAX || x < MIN) {
			throw new IllegalStateException( "Value of x is " + x );
		}
		if (y > MAX || y < MIN) {
			throw new IllegalStateException( "Value of y is " + y );
		}
	}

	private float add (float a) {
		a += 0.25f;
		return a > 5f ? 5f : a;
	}

	private float minus (float a) {
		a -= 0.25f;
		return a < -5f ? -5f : a;
	}

	@Override
	public int compareTo (Member o) {
		if (fitness == 0) {
			calculateFitness();
		}
		if (o.getFitness() == 0) {
			calculateFitness();
		}
		return (int) (o.getFitness() - getFitness());
	}
}
