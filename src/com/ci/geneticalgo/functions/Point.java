package com.ci.geneticalgo.functions;

import com.ci.geneticalgo.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 9/5/2015.
 */
public abstract class Point implements Member {

	private final float maxY;
	private final float minY;
	float min;
	float max;
	float mutationFactor = 0.25f;

	float  x;
	float  y;
	double fitness;

	protected Point (float min, float max) {
		this.min = min;
		this.max = max;
		this.minY = min;
		this.maxY = max;
	}

	protected Point (float minX, float maxX, float minY, float maxY) {
		this.min = minX;
		this.max = maxX;
		this.minY = minY;
		this.maxY = maxY;
	}

	protected void initRandom (float min, float max) {
		x = Utils.randomFloatBetween( min, max );
		y = Utils.randomFloatBetween( min, max );
		calculateFitness();
	}

	protected void initWithGaussianRandom (float min, float max) {
		x = Utils.gaussianBetweenMinMax( min, max );
		y = Utils.gaussianBetweenMinMax( min, max );
		calculateFitness();
	}

	@Override
	public Member newMember () {
		Point point = newPoint();
		point.x = Utils.randomFloatBetween( min, max );
		point.y = Utils.randomFloatBetween( min, max );
		calculateFitness();
		return point;
	}

	public abstract void calculateFitness ();

	public abstract Point newPoint ();

	@Override
	public double getFitness () {
		return fitness;
	}

	@Override
	public List<Member> recombine (Member parent2) {
		float x1 = x, x2 = ((Point) parent2).x;
		float y1 = y, y2 = ((Point) parent2).y;

		Point point1 = newPoint();
		point1.x = x1;
		point1.y = y2;
		point1.calculateFitness();

		Point point2 = newPoint();
		point2.x = x2;
		point2.y = y1;
		point2.calculateFitness();

		List<Member> offsprings = new ArrayList<Member>( 2 );
		offsprings.add( point1 );
		offsprings.add( point2 );
		return offsprings;
	}

	@Override
	public Member mutate () {
		Point point = newPoint();
		point.x = x < 0 ? minus( x ) : add( x );
		point.y = y < 0 ? minus( y ) : add( y );
		return point;
	}

	@Override
	public Member gaussianMutation () {
		Point point = newPoint();

		float newX = x + Utils.gaussianBetweenMinMax( -1 * mutationFactor, mutationFactor );
		newX = newX > max ? max : newX < min ? min : newX;
		point.x = newX;
		float newY = y + Utils.gaussianBetweenMinMax( -1 * mutationFactor, mutationFactor );
		newY = newY > max ? max : newY < min ? min : newY;
		point.y = newY;

		return point;
	}

	@Override
	public String log () {
		StringBuilder builder = new StringBuilder();
		builder.append( "x:" ).append( x ).append( " y:" ).append( y ).append( " fit:" ).append( fitness );
		return builder.toString();
	}

	@Override
	public void checkValues () {
		if (x > max || x < min) {
			throw new IllegalStateException( "Value of x is " + x );
		}
		if (y > max || y < min) {
			throw new IllegalStateException( "Value of y is " + y );
		}
	}

	private float add (float a) {
		a += mutationFactor;
		return a > max ? max : a;
	}

	private float minus (float a) {
		a -= mutationFactor;
		return a < min ? min : a;
	}

	@Override
	public int compareTo (Member o) {
		if (fitness == 0) {
			calculateFitness();
		}
		if (o.getFitness() == 0) {
			calculateFitness();
		}
		return (int) (o.getFitness() - getFitness());
	}
}
