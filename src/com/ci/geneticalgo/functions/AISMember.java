package com.ci.geneticalgo.functions;

/**
 * Created by Trikster on 11/6/2015.
 */
public interface AISMember extends Member {

	AISMember newRandom ();
	AISMember mutate (float min, float max);
}
