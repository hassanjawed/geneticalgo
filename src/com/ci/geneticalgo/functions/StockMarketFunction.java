package com.ci.geneticalgo.functions;

import com.ci.geneticalgo.Utils;
import com.ci.geneticalgo.neural_network.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 10/29/2015.
 */
public class StockMarketFunction implements Member {


	float       fitness;
	List<Float> weights;
	private static int        weightCount;
	static         List<Data> data;
	static         int        neuronCount;
	private        int        weightIndex;
	private static Data       learningData;
	private static int currentDataIndex = -1;
	private static Selection selection;

	public enum Selection {
		RANDOM, LINEAR
	}

	public static void setSelection (Selection selection) {
		StockMarketFunction.selection = selection;
	}

	public static void setData (List<Data> data, int neuronCount) {
		if (data == null || data.size() <= 0) throw new IllegalArgumentException( "data can not be empty" );

		StockMarketFunction.data = data;
		StockMarketFunction.weightCount = (data.get( 0 ).inputs.length * neuronCount) + neuronCount; // +neuronCount
		// for output neuron
		StockMarketFunction.neuronCount = neuronCount;
	}

	public static StockMarketFunction newObject () {
		if(data == null) throw new IllegalStateException( "First call setData()" );

		StockMarketFunction obj = new StockMarketFunction();
		obj.randInitWeights();
		return obj;
	}

	private StockMarketFunction () {
		weights = new ArrayList<>();
	}

	@Override
	public Member newMember () {
		StockMarketFunction stockMarketFunction = newObject();
		stockMarketFunction.randInitWeights();
		return stockMarketFunction;
	}

	private void randInitWeights () {
		weights = new ArrayList<>( weightCount );
		for (int i = 0; i < weightCount; i++) {
			weights.add( Utils.randomFloatBetween( -2, 2 ) );
		}
	}

	@Override
	public void calculateFitness () {
		if (data == null || data.size() < 2) {
			throw new IllegalStateException( "Data not set or have less than 2 elements" );
		}
//		if (learningData == null) {
//			nextInput();
//			learningData = data.get( currentDataIndex );
//			if (selection == Selection.RANDOM) {
//				learningData = data.get( (int) Utils.randomFloatBetween( 0, StockMarketFunction.data.size() - 1 ) );
//			} else {
//				learningData = data.get( currentDataIndex = 0 );
//			}
//		}
		float sseSum = 0;
		for (Data d : data) {
			float output = calculateOutput( d.normInputs );
			sseSum += d.getSSE( output );
		}
		fitness = sseSum / data.size();
//		fitness = Math.abs( calculateOutput( normInputs1 ) - calculateOutput( normInputs2 ) );
	}

	private float calculateOutput (Float[] inputs) {
		List<Float> inputLayer = new ArrayList<>( inputs.length );
		for (int i = 0; i < inputs.length; i++) {
			inputLayer.add( inputs[i] * weights.get( i ) );
		}
		List<Float> neuronOutputs = new ArrayList<>();
		weightIndex = 0;
		for (int j = 0; j < neuronCount; j++) {
			neuronOutputs.add( j, 0f );
			float sum = 0;
			for (int i = 0; i < inputs.length; i++) {
				sum += inputs[i] * weights.get( weightIndex++ );
//				float val = weights.get( weightIndex++ ) * inputs[i];
//				val = neuronOutputs.get( j ) + val;
//				neuronOutputs.set( j, val );
			}
			neuronOutputs.set( j, Utils.sigmoid( sum ) );
		}
		float output = 0;
		for (int i = 0; i < neuronOutputs.size(); i++) {
			output += (neuronOutputs.get( i ) * weights.get( weightIndex++ ));
		}
		return Utils.sigmoid( output );
	}

	@Override
	public double getFitness () {
		return fitness;
	}

	@Override
	public List<Member> recombine (Member parent2) {
		StockMarketFunction firstParent = this;
		StockMarketFunction secondParent = (StockMarketFunction) parent2;
		StockMarketFunction child1 = new StockMarketFunction();
		StockMarketFunction child2 = new StockMarketFunction();

		int breakIndex = Utils.randomIntBetween( 1, weightCount - 1 );

		List<Float> child1Weights = new ArrayList<>( weightCount );
		List<Float> child2Weights = new ArrayList<>( weightCount );

		for (int i = 0; i < weightCount; i++) {
			if (i < breakIndex) {
				child1Weights.add( firstParent.weights.get( i ) );
				child2Weights.add( secondParent.weights.get( i ) );
			} else {
				child1Weights.add( secondParent.weights.get( i ) );
				child2Weights.add( firstParent.weights.get( i ) );
			}
		}

//		child1Weights.addAll( firstParent.weights.subList( 0, breakIndex ) );
//		child2Weights.addAll( secondParent.weights.subList( 0, breakIndex ) );
//		child1Weights.addAll( breakIndex + 1, secondParent.weights.subList( breakIndex + 1,
//				secondParent.weights.size() - 1 ) );
//		child2Weights.addAll( breakIndex + 1, firstParent.weights.subList( breakIndex + 1,
//				firstParent.weights.size() - 1 ) );

		child1.weights = child1Weights;
		child2.weights = child2Weights;

		List<Member> children = new ArrayList<>();
		children.add( child1 );
		children.add( child2 );
		return children;
	}

	@Override
	public Member mutate () {
		for (int i = 0; i < weights.size(); i++) {
			float v = Utils.randomFloatBetween( -0.5f, 0.5f );
			weights.set( i, weights.get( i ) + v );
		}
		return this;
	}

	@Override
	public Member gaussianMutation () {
		return null;
	}

	@Override
	public String log () {
		StringBuilder sb = new StringBuilder( "Fitness:" + fitness + " item:" + currentDataIndex + " " );
		for (Float weight : weights) {
			sb.append( weight ).append( ", " );
		}
		return sb.toString();
	}

	@Override
	public void checkValues () {
		// TODO check all weights used in fitness evaluation
//		learningData = data.get( currentDataIndex );
//		learningData = data.get( (int) Utils.randomFloatBetween( 0, StockMarketFunction.data.size() - 1 ) );
	}

	public static void nextInput () {
		if (selection == Selection.RANDOM) {
			currentDataIndex = (int) Utils.randomFloatBetween( 0, StockMarketFunction.data.size() - 1 );
		} else {
			currentDataIndex = currentDataIndex > data.size() ? 0 : currentDataIndex + 1;
		}
	}

	public static boolean haveLearnt () {
		return false;
//		return currentDataIndex == data.size() - 1;
	}

	@Override
	public int compareTo (Member o) {
		if (fitness == 0) {
			calculateFitness();
		}
		if (o.getFitness() == 0) {
			calculateFitness();
		}
		return (int) (o.getFitness() - getFitness());
	}

	public List<Float> getWeights () {
		return weights;
	}
}
