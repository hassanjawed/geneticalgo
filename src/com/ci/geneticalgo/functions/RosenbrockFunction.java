package com.ci.geneticalgo.functions;

import com.ci.geneticalgo.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 10/24/2015.
 */
public class RosenbrockFunction extends PointParticle {

	public static final int GENE_COUNT = 2;

	private RosenbrockFunction (List<Range> ranges) {
		super( GENE_COUNT, ranges );
	}

	public static RosenbrockFunction init () {
		Range range = new Range();
		range.max = 2;
		range.min = -2;

		Range range2 = new Range();
		range2.max = 3;
		range2.min = -1;

		List<Range> ranges = new ArrayList<>();
		ranges.add( range );
		ranges.add( range2 );
		RosenbrockFunction obj = new RosenbrockFunction( ranges );
		for (int i = 0; i < GENE_COUNT; i++) {
			obj.genes.add( Utils.randomFloatBetween( ranges.get( i ).min, ranges.get( i ).max ) );
		}
		return obj;
	}

	@Override
	protected PointMember newPoint () {
		return RosenbrockFunction.init();
	}

	@Override
	public void calculateFitness () {
		float x = getGeneAt( 0 );
		float y = getGeneAt( 1 );
		fitness = (100 * Utils.square( y - Utils.square( x ) )) + Utils.square( 1 - x );
	}

	@Override
	public AISMember newRandom () {
		return RosenbrockFunction.init();
	}
}
