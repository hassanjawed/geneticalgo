package com.ci.geneticalgo.functions;

import com.ci.geneticalgo.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 10/24/2015.
 */
public abstract class PointMember implements AISMember {

	float mutationFactor = 0.05f;
	static List<Range> ranges;
	int         genesCount;
	List<Float> genes;
	double      fitness;


	public PointMember (int genesCount, List<Range> ranges) {
		this.genesCount = genesCount;
		this.genes = new ArrayList<>( genesCount );
		PointMember.ranges = ranges;
	}

	@Override
	public Member newMember () {
		PointMember pointMember = newPoint();
		for (int i = 0; i < pointMember.genesCount; i++) {
			pointMember.genes.set( i, Utils.randomFloatBetween( ranges.get( i ).min, ranges.get( i ).max ) );
		}
		return pointMember;
	}

	@Override
	public double getFitness () {
		return fitness;
	}

	protected abstract PointMember newPoint ();

	public Float getGeneAt (int pos) {
		return genes.get( pos );
	}

	public void setGeneAt (int pos, float value) {
		if (value > maxAt( pos )) {
			value = maxAt( pos );
		} else if (value < minAt( pos )) {
			value = minAt( pos );
		}
		genes.set( pos, value );
	}

	@Override
	public List<Member> recombine (Member parent2) {

		PointMember member = (PointMember) parent2;

		PointMember p1 = newPoint();
		PointMember p2 = newPoint();

		p1.genes.set( 0, getGeneAt( 0 ) );
		p1.genes.set( 1, member.getGeneAt( 1 ) );

		p2.genes.set( 0, member.getGeneAt( 0 ) );
		p2.genes.set( 1, getGeneAt( 1 ) );

		List<Member> members = new ArrayList<>( 2 );
		members.add( p1 );
		members.add( p2 );

		return members;
	}

	@Override
	public AISMember mutate (float min, float max) {
		PointMember member = newPoint();

		for (int i = 0; i < genes.size(); i++) {
			member.genes.add( genes.get( i ) + Utils.randomFloatBetween( min, max ) );
		}
		return member;
	}

	@Override
	public Member mutate () {
		PointMember point = newPoint();
		point.setGeneAt( 0, getGeneAt( 0 ) < 0 ? minus( 0 ) : add( 0 ) );
		point.setGeneAt( 1, getGeneAt( 1 ) < 0 ? minus( 1 ) : add( 1 ) );
		return point;
	}

	private float add (int geneIndex) {
		float a = getGeneAt( geneIndex );
		float max = ranges.get( geneIndex ).max;

		a += mutationFactor;
		return a > max ? max : a;
	}

	private float minus (int geneIndex) {
		float a = getGeneAt( geneIndex );
		float min = ranges.get( geneIndex ).min;

		a -= mutationFactor;
		return a < min ? min : a;
	}

	@Override
	public Member gaussianMutation () {
		PointMember point = newPoint();

		float newX = getGeneAt( 0 ) + Utils.gaussianBetweenMinMax( -1 * mutationFactor, mutationFactor );
		newX = newX > maxAt( 0 ) ? maxAt( 0 ) : newX < minAt( 0 ) ? minAt( 0 ) : newX;
		point.setGeneAt( 0, newX );

		float newY = getGeneAt( 1 ) + Utils.gaussianBetweenMinMax( -1 * mutationFactor, mutationFactor );
		newY = newY > maxAt( 1 ) ? maxAt( 1 ) : newY < minAt( 1 ) ? minAt( 1 ) : newY;
		point.setGeneAt( 1, newY );

		return point;
	}

	private float maxAt (int index) {
		return ranges.get( index ).max;
	}

	private float minAt (int index) {
		return ranges.get( index ).min;
	}

	@Override
	public String log () {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < genesCount; i++) {
			builder.append( "g" ).append( i + 1 ).append( ": " ).append( getGeneAt( i ) ).append( ";" );
		}
		return builder.toString();
	}

	@Override
	public void checkValues () {
		for (int i = 0; i < genesCount; i++) {
			Float geneAt = getGeneAt( i );
			if (geneAt > maxAt( i ) || geneAt < minAt( i )) {
				throw new IllegalStateException( "Value of gene" + i + " is " + geneAt );
			}
		}
	}

	@Override
	public int compareTo (Member o) {
		if (fitness == 0) {
			calculateFitness();
		}
		if (o.getFitness() == 0) {
			calculateFitness();
		}
		return (int) (o.getFitness() - getFitness());
	}
}
