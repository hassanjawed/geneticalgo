package com.ci.geneticalgo.functions;

import java.util.List;

/**
 * Created by Trikster on 9/6/2015.
 */
public interface Member extends Comparable<Member> {

	Member newMember ();

	void calculateFitness ();

	double getFitness ();

	List<Member> recombine (Member parent2);

	Member mutate ();

	Member gaussianMutation ();

	String log ();

	void checkValues ();
}
