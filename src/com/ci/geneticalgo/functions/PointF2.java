package com.ci.geneticalgo.functions;

import com.ci.geneticalgo.Utils;

import java.util.*;

/**
 * Created by Trikster on 9/7/2015.
 */
public class PointF2 implements Member {

	public static final float MIN_X = -2f;
	public static final float MAX_X = 2f;
	public static final float MIN_Y = -1f;
	public static final float MAX_Y = 3f;

	float  x;
	float  y;
	double fitness;

	private PointF2 () {}

	public static PointF2 initRandom () {
		PointF2 p = new PointF2();
		p.x = Utils.randomFloatBetween( MIN_X, MAX_X );
		p.y = Utils.randomFloatBetween( MIN_Y, MAX_Y );
		p.fitness = 0;
		return p;
	}

	@Override
	public Member newMember () {
		return PointF2.initRandom();
//		x = Utils.randomFloatBetween( MIN_X, MAX_X );
//		y = Utils.randomFloatBetween( MIN_Y, MAX_Y );
//		fitness = 0;
	}

	@Override
	public void calculateFitness () {
		fitness = (100 * Utils.square( Utils.square( x ) - y )) + Utils.square( 1 - x );
	}

	@Override
	public double getFitness () {
		return fitness;
	}

	@Override
	public List<Member> recombine (Member parent2) {
		float x1 = x, x2 = ((PointF2) parent2).x;
		float y1 = y, y2 = ((PointF2) parent2).y;

		PointF2 point1 = new PointF2();
		point1.x = x1;
		point1.y = y2;
		point1.calculateFitness();

		PointF2 point2 = new PointF2();
		point2.x = x2;
		point2.y = y1;
		point2.calculateFitness();

		List<Member> offsprings = new ArrayList<Member>( 2 );
		offsprings.add( point1 );
		offsprings.add( point2 );
		return offsprings;
	}

	@Override
	public Member mutate () {
		PointF2 point = new PointF2();

		point.x = x < 0 ? minus( x, MIN_X, MAX_X ) : add( x, MIN_X, MAX_X );
		point.y = y < 0 ? minus( y, MIN_Y, MAX_Y ) : add( y, MIN_Y, MAX_Y );

//		point.x = Math.random() > 0.5f ? add( x, MIN_X, MAX_X ) : minus( x, MIN_X, MAX_X );
//		point.y = Math.random() > 0.5f ? minus( y, MIN_Y, MAX_Y ) : add( y, MIN_Y, MAX_Y );

		return point;
	}

	@Override
	public Member gaussianMutation () {
		PointF2 point = new PointF2();
		float mutationFactor = 0.25f;
		float newX = x + Utils.gaussianBetweenMinMax( -1 * mutationFactor, mutationFactor );
		newX = newX > MAX_X ? MAX_X : newX < MIN_X ? MIN_X : newX;
		point.x = newX;
		float newY = y + Utils.gaussianBetweenMinMax( -1 * mutationFactor, mutationFactor );
		newY = newY > MAX_Y ? MAX_Y : newY < MIN_Y ? MIN_Y : newY;
		point.y = newY;

		return point;
	}

	private float add (float a, float min, float max) {
		return a + 0.75f > max ? max : a + 0.75f;
	}

	private float minus (float a, float min, float max) {
		return a - 0.75f < min ? min : a - 0.75f;
	}

	@Override
	public String log () {
		StringBuilder builder = new StringBuilder();
		builder.append( "x:" ).append( x ).append( " y:" ).append( y ).append( " fit:" ).append( fitness );
		return builder.toString();
	}

	@Override
	public void checkValues () {
		if (x > MAX_X || x < MIN_X) {
			throw new IllegalStateException( "Value of x is " + x );
		}
		if (y > MAX_Y || y < MIN_Y) {
			throw new IllegalStateException( "Value of y is " + y );
		}
	}

	@Override
	public int compareTo (Member o) {
		if (fitness == 0) {
			calculateFitness();
		}
		if (o.getFitness() == 0) {
			calculateFitness();
		}
		return (int) (o.getFitness() - getFitness());
	}
}
