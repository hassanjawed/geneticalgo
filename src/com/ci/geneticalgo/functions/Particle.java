package com.ci.geneticalgo.functions;

/**
 * Created by Trikster on 10/24/2015.
 */
public interface Particle extends Member {

	public void checkPBest ();

	public void updateVelocity (PointParticle particle);
}
