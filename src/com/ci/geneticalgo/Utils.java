package com.ci.geneticalgo;

import java.util.Random;

/**
 * Created by Trikster on 9/6/2015.
 */
public class Utils {

	public static int randomIntBetween (int min, int max) {
		Random random = new Random();
		int i = random.nextInt( max - min );
		return i + min;
	}

	public static float sigmoid (double x) {
		return (float) (1 / (1 + Math.pow( Math.E, (-1 * x) )));
	}

	public static float hyperbolicTangent (double x) {
		return (float) (2 * Utils.sigmoid( x ) * 2 * x - 1);
	}

	public static float randomFloatBetween (float min, float max) {
		double distance = Math.sqrt( square( max - min ) );
		double random = Math.random() * distance;
		return (float) (random - Math.abs( min ));
	}

	public static boolean randomBoolean () {
		return Math.random() < 0.5f;
	}

	public static float gaussianBetweenMinMax (float min, float max) {
		Random random = new Random();
		double v;
		do {
			v = random.nextGaussian();
		} while (v < -1 || v > 1);
		v = 0.5 * v + 0.5;
		float value = (float) v;
		double distance = Math.sqrt( square( max - min ) );
		return (float) ((distance * value) - Math.abs( min ));
	}

	public static double square (double a) {
		return a * a;
	}
}
