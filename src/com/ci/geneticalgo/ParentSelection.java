package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.List;

/**
 * Created by Trikster on 9/6/2015.
 */
public interface ParentSelection {

	void selectParentsFrom (List<Member> members);

	/**
	 * List of selected parents
	 * @return
	 */
	List<Member> getParents ();

	/**
	 * Apply recombination on selected members
	 */
	void performRecombinition ();


	/**
	 * Apply mutation on selected parents
	 */
	void performMutation ();

	/**
	 *
	 * @return offsprings + current generation
	 */
	List<Member> getPopulation ();

	/**
	 * Offsprings produced by selected parent's recombination and mutation
	 * @return
	 */
	List<Member> getOffsprings ();
}
