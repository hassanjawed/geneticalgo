package com.ci.geneticalgo.world;

import java.util.List;

/**
 * Created by Trikster on 11/3/2015.
 */
public interface World<T> {

	void add (T item);

	void addAll (List<T> items);

	List<T> getPopulation ();

	void beginNew ();

	void set (List<T> items);

	T getRecentBestIndividual ();

	double averageRecentGenerationFitness ();
}
