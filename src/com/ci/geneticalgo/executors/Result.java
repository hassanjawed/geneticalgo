package com.ci.geneticalgo.executors;

import com.ci.geneticalgo.functions.Member;
import com.ci.geneticalgo.world.World;

/**
 * Created by Trikster on 11/3/2015.
 */
public class Result<T> {

	float bestSoFar;
	float averageFitness;
	int   populationSize;

	private Result () {

	}

	public float getBestSoFar () {
		return bestSoFar;
	}

	public float getAverageFitness () {
		return averageFitness;
	}

	public int getPopulationSize () {
		return populationSize;
	}

	public static <T extends Member> Result<T> calculate (World<T> world) {
		Result<T> result = new Result<>();
		result.populationSize = world.getPopulation().size();
		result.bestSoFar = (float) world.getRecentBestIndividual().getFitness();
		result.averageFitness = (float) world.averageRecentGenerationFitness();
		return result;
	}

	public String log () {
		return String.format( "bsf: %f avgFit: %f size:%d", bestSoFar, averageFitness, populationSize );
	}
}
