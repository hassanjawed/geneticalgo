package com.ci.geneticalgo.executors;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 11/3/2015.
 */
public class ResultStore {

	String             title;
	String             imageName;
	List<List<Result>> results;

	public ResultStore (int executionCount) {
		results = new ArrayList<>();
		for (int i = 0; i < executionCount; i++) {
			results.add( new ArrayList<>() );
		}
	}

	public String getTitle () {
		return title;
	}

	public void setTitle (String title) {
		this.title = title;
	}

	public String getImageName () {
		return imageName;
	}

	public void setImageName (String imageName) {
		this.imageName = imageName;
	}

	public void addGenerationResult (int iteration, Result generationOutcome) {
		results.get( iteration ).add( generationOutcome );
	}

	public List<Float> calculateAverageBestSoFar () {
		List<Float> avgBSF = new ArrayList<>();
		int generationCount = results.get( 0 ).size();

		for (int i = 0; i < generationCount; i++) {
			float fitnessSum = 0;
			int generations = results.size();
			for (int j = 0; j < generations; j++) {
				fitnessSum += results.get( j ).get( i ).getBestSoFar();
			}
			avgBSF.add( fitnessSum / generations );
		}
		return avgBSF;
	}

	public List<Float> calculateAverageAverageFitness () {
		List<Float> avgFitness = new ArrayList<>();
		int generationCount = results.get( 0 ).size();

		for (int i = 0; i < generationCount; i++) {
			float fitnessSum = 0;
			int generations = results.size();
			for (int j = 0; j < generations; j++) {
				fitnessSum += results.get( j ).get( i ).getAverageFitness();
			}
			avgFitness.add( fitnessSum / generations );
		}
		return avgFitness;
	}
}
