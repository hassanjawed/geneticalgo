package com.ci.geneticalgo.executors;

import com.ci.geneticalgo.functions.Member;
import com.ci.geneticalgo.world.World;

/**
 * Created by Trikster on 11/3/2015.
 */
public abstract class Executor<T> {

	protected World<T>         world;
	protected ResultStore   resultStore;
	protected Configuration config;
	protected int currentIteration = -1;
	private OnOptimizedIndividualFoundListener<T> trainedBotListener;

	public Executor (World<T> world, Configuration config) {
		this.world = world;
		this.config = config;
		resultStore = new ResultStore( config.executionCount );
	}

	public int getCurrentIteration() {
		return currentIteration;
	}

	public void setOptimizedMemberListener(OnOptimizedIndividualFoundListener<T> trainedBotListener) {
		this.trainedBotListener = trainedBotListener;
	}

	public ResultStore execute () {
		for (int i = 0; i < config.executionCount; i++) {
			currentIteration = i;
			world.beginNew();
			Result result = initialResult();
			System.out.println( "1: " + result.log() );
			resultStore.addGenerationResult( i, result );
			for (int j = 0; j < config.generations - 1; j++) {
				Result generationOutcome = executeGeneration();
				resultStore.addGenerationResult( i, generationOutcome );
				System.out.println( (j + 2) + ": " + generationOutcome.log() );
			}
			trainedBotListener.optimizedMember(world.getRecentBestIndividual());
		}
		return resultStore;
	}

	protected abstract Result initialResult ();

	protected abstract Result executeGeneration ();

	public static class Configuration {

		int executionCount;
		int generations;

		public Configuration (int executionCount, int generations) {
			this.executionCount = executionCount;
			this.generations = generations;
		}
	}

	public interface OnOptimizedIndividualFoundListener<T> {
		void optimizedMember(T member);
	}
}
