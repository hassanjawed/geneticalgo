package com.ci.geneticalgo.executors;

import com.ci.geneticalgo.functions.PointParticle;
import com.ci.geneticalgo.world.World;

import java.util.*;

/**
 * Created by Trikster on 11/6/2015.
 */
public class PSOExecutor extends Executor<PointParticle> {

	private PointParticle globalBestMember;
	private double        gBest;

	public PSOExecutor (World<PointParticle> world, Configuration config) {
		super( world, config );
	}

	@Override
	protected Result initialResult () {
		List<PointParticle> sortedMembers = new ArrayList<>( world.getPopulation() );
		Collections.sort( sortedMembers );
		globalBestMember = sortedMembers.get( 0 );
		gBest = globalBestMember.getFitness();

		return Result.calculate( world );
	}

	@Override
	protected Result executeGeneration () {
		for (PointParticle particle : world.getPopulation()) {
			particle.checkPBest();
			particle.updateVelocity( globalBestMember );
			particle.calculateFitness();
			if (particle.getFitness() > globalBestMember.getFitness()) {
				globalBestMember = particle;
			}
		}
		return Result.calculate( world );
	}
}
