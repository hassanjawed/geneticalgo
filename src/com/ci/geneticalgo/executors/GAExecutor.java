package com.ci.geneticalgo.executors;

import com.ci.geneticalgo.algos.Selector;
import com.ci.geneticalgo.functions.Member;
import com.ci.geneticalgo.world.World;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Trikster on 11/6/2015.
 */
public class GAExecutor extends Executor<Member> {

	public static final int PARENTS_COUNT = 2;

	public GAExecutor (World<Member> world, GAConfiguration config) {
		super( world, config );
	}

	@Override
	protected Result initialResult () {
		return Result.calculate( world );
	}

	@Override
	protected Result executeGeneration () {
//		Step 1: Initialize the population randomly or with potentially
//		good solutions.
//		Step 2: Compute the fitness of each individual in the
//		population.
		calculateFitness();

//		Step 3: Select parents using a selection procedure.
		Selector parentSelector = ((GAConfiguration) config).ps;
		List<Member> parents = parentSelector.selectIndividualsFrom( world.getPopulation(), PARENTS_COUNT );

//		Step 4: Create offspring by crossover and mutation operators.
		List<Member> offspring = parents.get( 0 ).recombine( parents.get( 1 ) );
		List<Member> mutation = parentSelector
				.selectIndividualsFrom( world.getPopulation(), (int) Math.ceil( 0.1f * world.getPopulation().size() ) );
		offspring.addAll( mutation.stream().map( Member::mutate ).collect( Collectors.toList() ) );

//		Step 5: Compute the fitness of the new offspring.
		offspring.forEach( com.ci.geneticalgo.functions.Member::calculateFitness );

//		Step 6: Select members of population to die using a selection procedure.
		offspring.addAll( world.getPopulation() );
		List<Member> newPopulation = ((GAConfiguration) config).ss
				.selectIndividualsFrom( offspring, world.getPopulation().size() );
		Result<Member> result = Result.calculate( world );
		world.set( newPopulation );
		return result;
	}

	private void calculateFitness () {
		world.getPopulation().forEach( com.ci.geneticalgo.functions.Member::calculateFitness );
	}

	public static class GAConfiguration extends Configuration {

		Selector ps;
		Selector ss;

		public GAConfiguration (int executionCount, int generations, Selector parentSelector, Selector
				survivalSelector) {
			super( executionCount, generations );
			this.ps = parentSelector;
			this.ss = survivalSelector;
		}
	}
}

