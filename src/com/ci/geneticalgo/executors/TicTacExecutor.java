package com.ci.geneticalgo.executors;

import com.ci.geneticalgo.algos.Selector;
import com.ci.geneticalgo.functions.Member;
import com.ci.geneticalgo.functions.TicTacBot;
import com.ci.geneticalgo.world.World;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Trikster on 11/6/2015.
 */
public class TicTacExecutor extends Executor<Member> {

	public static final int PARENTS_COUNT = 2;
	public static final int CLONES_COUNT = 2;
	public static final int RANDOM_COUNT = 2;

	private TicTacBot fitBot;
	private OnFitBotFoundListener botFoundListener;

	public TicTacExecutor (World<Member> world, GAConfiguration config) {
		super( world, config );
	}

	public void setBotFoundListener (OnFitBotFoundListener botFoundListener) {
		this.botFoundListener = botFoundListener;
	}

	@Override
	protected Result initialResult () {
		fitBot = (TicTacBot) world.getPopulation().get( 0 );
		return Result.calculate( world );
	}

	@Override
	protected Result executeGeneration () {
		// Select 2 bots through Tournament from population -> P
		Selector parentSelector = ((GAConfiguration) config).ps;
		List<Member> parents = parentSelector.selectIndividualsFrom( world.getPopulation(), PARENTS_COUNT );

		// do crossover and mutation on them to produce offsprings -> O
		List<Member> offspring = parents.get( 0 ).recombine( parents.get( 1 ) );
		List<Member> mutation = parentSelector
				.selectIndividualsFrom( world.getPopulation(), (int) Math.ceil( 0.1f * world.getPopulation().size() ) );
		offspring.addAll( mutation.stream().map( Member::mutate ).collect( Collectors.toList() ) );

		// add 2 new random bots -> R
		for (int i = 0; i < RANDOM_COUNT; i++) {
			offspring.add( parents.get( 0 ).newMember() );
		}

		// select new P through Tournament survival from P, O & R
		offspring.addAll( world.getPopulation() );
		TicTacBot bsf = (TicTacBot) world.getRecentBestIndividual();

		for (int i = 0; i < CLONES_COUNT; i++) {
			TicTacBot tempBot = (TicTacBot) bsf.newMember();
			for (int j = 0; j < tempBot.getWeights().size(); j++) {
				tempBot.getWeights().set( j, tempBot.getWeights().get( j ) );
			}
			offspring.add( tempBot.mutate() );
		}

		List<Member> newPopulation = ((GAConfiguration) config).ss
				.selectIndividualsFrom( offspring, world.getPopulation().size() );
		Result<Member> result = Result.calculate( world );
		if (bsf.getFitness() > fitBot.getFitness()) {
			fireFitBotFound( bsf );
		}
		world.set( newPopulation );
		return result;
	}

	private void fireFitBotFound(Member recentBestIndividual) {
		if (botFoundListener != null) {
			botFoundListener.onBotFound(getCurrentIteration(), (TicTacBot) recentBestIndividual);
		}
	}

	public interface OnFitBotFoundListener {

		void onBotFound (int currentIteration, TicTacBot fitBot);
	}

	public static class GAConfiguration extends Configuration {

		Selector ps;
		Selector ss;

		public GAConfiguration (int executionCount, int generations, Selector parentSelector, Selector
				survivalSelector) {
			super( executionCount, generations );
			this.ps = parentSelector;
			this.ss = survivalSelector;
		}
	}
}

