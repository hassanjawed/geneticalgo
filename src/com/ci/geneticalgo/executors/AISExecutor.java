package com.ci.geneticalgo.executors;

import com.ci.geneticalgo.algos.NRankBasedSelector;
import com.ci.geneticalgo.algos.StrictTruncation;
import com.ci.geneticalgo.functions.AISMember;
import com.ci.geneticalgo.world.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 11/3/2015.
 */
public class AISExecutor extends Executor<AISMember> {

	public static final int POPULATION_SIZE_P         = 10;
	public static final int SUB_POPULATION_SIZE_F     = 5;
	public static final int CLONES_COUNT_S            = 3;
	public static final int RANDOM_POPULATION_COUNT_R = 3;

	public static final float MUTATION_FACTOR = 0.05f;

	public AISExecutor (World<AISMember> world, int generation) {
		super( world, new Configuration( POPULATION_SIZE_P, generation ) );
		System.setProperty( "java.util.Arrays.useLegacyMergeSort", "true" );
	}

	@Override
	protected Result initialResult () {
		return Result.calculate( world );
	}

	@Override
	protected Result executeGeneration () {
//		// Select F(5) best solutions from P
//		NRankBasedSelector nRBS = new NRankBasedSelector();
//		List<AISMember> fMembers = nRBS.selectIndividualsFrom( world.getPopulation(), SUB_POPULATION_SIZE_F );
//
//		// Produce more clones from better solutions
//		// Mutate each clone inversely less change in best solution's clone
//		List<AISMember> clones = new ArrayList<>();
//		for (int i = 0; i < fMembers.size(); i++) {
//			float scaledMutation = MUTATION_FACTOR / (fMembers.size() - i);
//			for (int j = fMembers.size(); j > i; j--) {
//				clones.add( fMembers.get( i ).mutate( scaledMutation * -1, scaledMutation ) );
//			}
//		}

		NRankBasedSelector nRBS = new NRankBasedSelector();
		List<AISMember> fMembers = nRBS.selectIndividualsFrom( world.getPopulation(), SUB_POPULATION_SIZE_F );
		List<AISMember> clones = new ArrayList<>( fMembers );

		for (int i = 0; i < clones.size(); i++) {
			List<AISMember> selectedClones = nRBS.selectIndividualsFrom( clones, 1 );
			int index = clones.indexOf( selectedClones.get( 0 ) );
			AISMember selectedClone = clones.get( index );
			selectedClone.mutate( -MUTATION_FACTOR, MUTATION_FACTOR );
			clones.set( index, selectedClone );
		}

		// Select S(3) clones
		world.addAll( nRBS.selectIndividualsFrom( clones, CLONES_COUNT_S ) );

		// Create new random R(3) members
		for (int i = 0; i < RANDOM_POPULATION_COUNT_R; i++) {
			world.add( world.getPopulation().get( 0 ).newRandom() );
		}

		// Replace poorer members of P with better solutions from S and R.
		StrictTruncation truncation = new StrictTruncation();
		List<AISMember> newPopulation = truncation.selectIndividualsFrom( world.getPopulation(), POPULATION_SIZE_P );
		world.set( newPopulation );
		return Result.calculate( world );
	}
}
