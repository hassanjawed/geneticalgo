package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 10/10/2015.
 */
public abstract class EASelector implements ParentSelection{

	protected Member parent1;
	protected Member parent2;

	private List<Member> offsprings;

	public EASelector () {
		offsprings = new ArrayList<Member>();
	}

	@Override
	public List<Member> getParents () {
		List<Member> parents = new ArrayList<Member>();
		parents.add( parent1 );
		parents.add( parent2 );

		return parents;
	}

	@Override
	public void performRecombinition () {
		List<Member> recombine = parent1.recombine( parent2 );
		for (Member member : recombine) {
			member.calculateFitness();
		}
		offsprings.addAll( recombine );
	}

	@Override
	public void performMutation () {
		Member mutant = parent1.mutate();
		mutant.calculateFitness();
		offsprings.add( mutant );
		Member mutant2 = parent2.mutate();
		mutant2.calculateFitness();
		offsprings.add( mutant2 );
	}

	@Override
	public List<Member> getPopulation () {
		List<Member> wholePopulation = new ArrayList<Member>();
		wholePopulation.add( parent1 );
		wholePopulation.add( parent2 );
		wholePopulation.addAll( offsprings );
		return wholePopulation;
	}

	@Override
	public List<Member> getOffsprings () {
		return offsprings;
	}
}
