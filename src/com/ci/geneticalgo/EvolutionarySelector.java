package com.ci.geneticalgo;

import com.ci.geneticalgo.functions.Member;

import java.util.*;

/**
 * Created by Trikster on 10/10/2015.
 */
public class EvolutionarySelector implements ParentSelection {

	private List<Member> population;

	private List<Member> offsprings;

	public EvolutionarySelector () {
		offsprings = new ArrayList<Member>();
	}

	@Override
	public void selectParentsFrom (List<Member> members) {
		population = members;
	}

	@Override
	public List<Member> getParents () {
		return population;
	}

	@Override
	public void performRecombinition () {
		for (int i = 0; i < population.size(); i += 2) {
			List<Member> recombine = population.get( i ).recombine( population.get( i + 1 ) );
			for (Member member : recombine) {
				member.calculateFitness();
			}
			offsprings.addAll( recombine );
		}
	}

	@Override
	public void performMutation () {
		List<Member> parents = new ArrayList<Member>( population );
		Collections.sort( parents );
		for (int i = 0; i < 6; i++) {
			Member member = parents.get( i );
			member.calculateFitness();
			offsprings.add( member.gaussianMutation() );
		}
	}

	@Override
	public List<Member> getPopulation () {
		List<Member> allPopulation = new ArrayList<Member>( population );
		allPopulation.addAll( offsprings );

		return allPopulation;
	}

	@Override
	public List<Member> getOffsprings () {
		return offsprings;
	}
}
