package com.ci.geneticalgo.algos;

import com.ci.geneticalgo.Utils;
import com.ci.geneticalgo.functions.Member;
import com.ci.geneticalgo.functions.TicTacBot;
import com.ci.geneticalgo.tictactoe.TicTacController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hassanjawed on 11/26/15.
 */
public class TicTacTournament implements Selector {

  public static final int MATCHES_IN_SERIES = 5;

  private static TicTacController controller;

  public static void setController(TicTacController controller) {
    TicTacTournament.controller = controller;
  }

  @Override
  public <T extends Member> List<T> selectIndividualsFrom(List<T> members, int selectionSize) {
    List<Member> selectedMembers = new ArrayList<>();
    int p1WinCount = 0, p2WinCount = 0;

    while (selectedMembers.size() < selectionSize) {
      int index1 = Utils.randomIntBetween( 0, members.size() - 1 );
      int index2 = Utils.randomIntBetween( 0, members.size() - 1 );
      TicTacBot member1 = (TicTacBot) members.get( index1 );
      TicTacBot member2 = (TicTacBot) members.get( index2 );

      for (int i = 0; i < MATCHES_IN_SERIES; i++) {
        TicTacBot ticTacBot = controller.evaluateFitness(member1, member2);
        if (ticTacBot == member1) {
          p1WinCount += 1;
        } else {
          p2WinCount += 1;
        }
      }

      selectedMembers.add( member1.getFitness() > member2.getFitness() ? member1 : member2 );

    }
    return (List<T>) selectedMembers;
  }
}
