package com.ci.geneticalgo.algos;

import com.ci.geneticalgo.functions.Member;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 11/6/2015.
 */
public class NFitnessProportionSelector implements Selector {

	@Override
	public <T extends Member> List<T> selectIndividualsFrom (List<T> members, int selectionSize) {
		double sum = 0;
		List<ProbRange> maxProb = new ArrayList<>( members.size() );
		for (Member member : members) sum += member.getFitness();

		float tempProb = 0, temp;

		for (Member member : members) {
			maxProb.add( new ProbRange( tempProb, temp = (float) (member.getFitness() / sum) ) );
			tempProb = temp;
		}

		List<T> selectedIndividuals = new ArrayList<>();
		T tempIndividual;
		for (int i = 0; i < selectionSize; i++) {
			do {
				double random1 = Math.random();
				tempIndividual = selectParent( members, maxProb, random1 );
			} while (tempIndividual == null);
			selectedIndividuals.add( tempIndividual );
		}

		return selectedIndividuals;
	}

	private <T extends Member> T selectParent (List<T> members, List<ProbRange> maxProb, double random) {
		for (int i = 0; i < members.size(); i++) {
			ProbRange thisProb = maxProb.get( i );
			if (random > thisProb.min && random <= thisProb.max) {
				return members.get( i );
			}
		}
		return null;
	}

	private static class ProbRange {

		float min;
		float max;

		public ProbRange (float min, float max) {
			this.min = min;
			this.max = max;
		}
	}
}
