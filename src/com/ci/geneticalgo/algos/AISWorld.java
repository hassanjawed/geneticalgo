package com.ci.geneticalgo.algos;

import com.ci.geneticalgo.functions.AISMember;
import com.ci.geneticalgo.world.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 11/6/2015.
 */
public class AISWorld implements World<AISMember> {

	List<AISMember> population;
	int             populationSize;
	private AISMember bestMember;
	private float averageFitness;

	public AISWorld (List<AISMember> population, int populationSize) {
		this.population = population;
		this.populationSize = populationSize;
	}

	@Override
	public void add (AISMember item) {
		population.add( item );
	}

	@Override
	public void addAll (List<AISMember> items) {
		population.addAll( items );
	}

	@Override
	public  List<AISMember> getPopulation () {
		return population;
	}

	@Override
	public void beginNew () {
		AISMember item = population.get( 0 );
		population = new ArrayList<>();
		for (int i = 0; i < populationSize; i++) {
			AISMember e = item.newRandom();
			e.calculateFitness();
			population.add( e );

		}
	}

	@Override
	public void set (List<AISMember> items) {
		population = new ArrayList<>( items );
	}

	@Override
	public AISMember getRecentBestIndividual () {
		bestMember = population.get( 0 );
		float fitnessSum = 0f;
		for (AISMember member : population) {
			if (member.getFitness() > this.bestMember.getFitness()) {
				this.bestMember = member;
			}
			fitnessSum += member.getFitness();
		}
		averageFitness = fitnessSum / population.size();
		return bestMember;
	}

	@Override
	public double averageRecentGenerationFitness () {
		if (averageFitness == 0) {
			getRecentBestIndividual();
		}
		return averageFitness;
	}
}
