package com.ci.geneticalgo.algos;

import com.ci.geneticalgo.functions.Member;

import java.util.*;

/**
 * Created by Trikster on 11/6/2015.
 */
public class NRankBasedSelector implements Selector {

	public <T extends Member> List<T> selectIndividualsFrom (List<T> members, int selectionSize) {
		if (selectionSize > members.size()) {
			throw new IllegalArgumentException( "Selection size cannot exceed members size" );
		}

		List<T> allMembers = new ArrayList<>( members );
		Collections.sort( allMembers );
		List<T> selectedMembers = new ArrayList<>();

		float lastP = 0;
		float p = 1f / members.size();
		List<ProbRange> ranges = new ArrayList<>( members.size() );
		for (int i = 0; i < members.size(); i++) {
			ranges.add( new ProbRange( lastP, lastP + p ) );
			lastP += p;
		}
		T tempMember;
		for (int i = 0; i < selectionSize; i++) {
			do {
				tempMember = selectParent( allMembers, ranges, Math.random() );
			} while (tempMember == null);
			selectedMembers.add( tempMember );
		}
		return selectedMembers;
	}

	private <T extends Member> T selectParent (List<T> members, List<ProbRange> maxProb, double random) {
		for (int i = 0; i < members.size(); i++) {
			ProbRange thisProb = maxProb.get( i );
			if (random > thisProb.min && random <= thisProb.max) {
				return members.get( i );
			}
		}
		return null;
	}


	private static class ProbRange {

		float min;
		float max;

		public ProbRange (float min, float max) {
			this.min = min;
			this.max = max;
		}
	}
}
