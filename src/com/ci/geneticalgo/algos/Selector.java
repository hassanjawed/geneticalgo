package com.ci.geneticalgo.algos;

import com.ci.geneticalgo.functions.Member;

import java.util.List;

/**
 * Created by Trikster on 11/6/2015.
 */
public interface Selector {

	<T extends Member> List<T> selectIndividualsFrom (List<T> members, int selectionSize);
}
