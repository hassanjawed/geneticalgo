package com.ci.geneticalgo.algos;

import com.ci.geneticalgo.functions.Member;
import com.ci.geneticalgo.world.World;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 11/6/2015.
 */
public class TicTacBotsWorld implements World<Member> {

	List<Member> population;
	int          populationSize;
	private Member bestMember;
	private float  averageFitness;

	public TicTacBotsWorld(List<? extends Member> population, int populationSize) {
		this.population = new ArrayList<>( population );
		this.populationSize = populationSize;
	}

	@Override
	public void add (Member item) {
		population.add( item );
	}

	@Override
	public void addAll (List<Member> items) {
		population.addAll( items );
	}

	@Override
	public List<Member> getPopulation () {
		return population;
	}

	@Override
	public void beginNew () {
		Member item = population.get( 0 );
		population = new ArrayList<>();
		for (int i = 0; i < populationSize; i++) {
			Member e = item.newMember();
			population.add( e );
		}
	}

	@Override
	public void set (List<Member> items) {
		population = new ArrayList<>( items );
	}

	@Override
	public Member getRecentBestIndividual () {
		bestMember = population.get( 0 );
		float fitnessSum = 0f;
		for (Member member : population) {
			if (member.getFitness() > this.bestMember.getFitness()) {
				this.bestMember = member;
			}
			fitnessSum += member.getFitness();
		}
		averageFitness = fitnessSum / population.size();
		return bestMember;
	}

	@Override
	public double averageRecentGenerationFitness () {
		if (averageFitness == 0) {
			getRecentBestIndividual();
		}
		return averageFitness;
	}
}
