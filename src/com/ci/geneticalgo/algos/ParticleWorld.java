package com.ci.geneticalgo.algos;

import com.ci.geneticalgo.functions.Member;
import com.ci.geneticalgo.functions.PointParticle;
import com.ci.geneticalgo.world.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trikster on 11/6/2015.
 */
public class ParticleWorld implements World<PointParticle> {

	List<PointParticle> population;
	int                 populationSize;
	private PointParticle bestMember;
	private float         averageFitness;

	public ParticleWorld (List<PointParticle> population, int populationSize) {
		this.population = population;
		this.populationSize = populationSize;
	}

	@Override
	public void add (PointParticle item) {
		population.add( item );
	}

	@Override
	public void addAll (List<PointParticle> items) {
		population.addAll( items );
	}

	@Override
	public List<PointParticle> getPopulation () {
		return population;
	}

	@Override
	public void beginNew () {
		Member item = population.get( 0 );
		population = new ArrayList<>();
		for (int i = 0; i < populationSize; i++) {
			Member e = item.newMember();
			e.calculateFitness();
			population.add( (PointParticle) e );
		}
	}

	@Override
	public void set (List<PointParticle> items) {
		population = new ArrayList<>( items );
	}

	@Override
	public PointParticle getRecentBestIndividual () {
		bestMember = population.get( 0 );
		float fitnessSum = 0f;
		for (PointParticle member : population) {
			if (member.getFitness() > this.bestMember.getFitness()) {
				this.bestMember = member;
			}
			fitnessSum += member.getFitness();
		}
		averageFitness = fitnessSum / population.size();
		return bestMember;
	}

	@Override
	public double averageRecentGenerationFitness () {
		if (averageFitness == 0) {
			getRecentBestIndividual();
		}
		return averageFitness;
	}
}
