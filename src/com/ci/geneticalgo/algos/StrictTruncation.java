package com.ci.geneticalgo.algos;

import com.ci.geneticalgo.functions.Member;

import java.util.*;

/**
 * Created by Trikster on 11/6/2015.
 */
public class StrictTruncation implements Selector {

	public <T extends Member> List<T> selectIndividualsFrom (List<T> individuals, int populationSize) {
		if (populationSize > individuals.size()) {
			throw new IllegalArgumentException( "Selection size exceeds individuals size" );
		}
		ArrayList<T> members = new ArrayList<>( individuals );
		Collections.sort( members );
		return members.subList( 0, populationSize );
	}
}
