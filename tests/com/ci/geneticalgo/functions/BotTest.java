package com.ci.geneticalgo.functions;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Trikster on 11/26/2015.
 */
public class BotTest {

	@Test
	public void testRecombination () {
		TicTacBot.setNeuronCount( 9, 9, 9 );
		TicTacBot bot1 = new TicTacBot();
		bot1 = (TicTacBot) bot1.newMember();
		TicTacBot bot2 = (TicTacBot) bot1.newMember();
		List<Member> recombine = bot1.recombine( bot2 );
		List<TicTacBot> parents = new ArrayList<>();
		parents.add( bot1 );
		parents.add( bot2 );
		for (int i = 0; i < recombine.size(); i++) {
			for (int j = 0; j < parents.size(); j++) {
				TicTacBot child = (TicTacBot) recombine.get( i );
				TicTacBot parent = parents.get( j );
				int cWS = child.getWeights().size();
				int pWS = parent.getWeights().size();
				assertTrue( cWS + "==" + pWS, cWS == pWS );
				int matchCount = 0;
				for (int k = 0; k < child.weights.size(); k++) {
					if (parent.getWeights().get( k ).equals( child.getWeights().get( k ) )) matchCount += 1;
				}
				assertTrue( matchCount < child.weights.size() );
			}
		}
	}
}
