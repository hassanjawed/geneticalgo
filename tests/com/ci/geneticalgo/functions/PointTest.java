package com.ci.geneticalgo.functions;


import com.ci.geneticalgo.Utils;
import com.ci.geneticalgo.neural_network.Data;
import org.apache.commons.csv.*;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

import static org.junit.Assert.assertTrue;

/**
 * Created by Trikster on 9/6/2015.
 */
public class PointTest {

	@Test
	public void testDataNormalization () throws IOException {
		List<Data> csvData = readData();
		for (Data data : csvData) {
			Float[] inputs = data.inputs;
			Float[] normInputs = data.normInputs;
			for (int i = 0; i < normInputs.length; i++) {
				Float normInput = normInputs[i];
				assertTrue( "Normalized inputs must be in 0-1 range " + normInput, normInput >= 0 && normInput <= 1 );
				float v = normInputs[i] * Data.diff + Data.min;
				float d = v - inputs[i];
				assertTrue( "Input " + inputs[i] + " OOR w/ norm " + normInputs[i] + " and denorm:" + v, d > -2 && d < 2 );
			}
			float y = data.normY * Data.diff + Data.min;
			float d = y - data.y;
			assertTrue( "Output " + data.y + " OOR w/ norm " + data.normY + " and denorm:" + y, d > -2 && d < 2 );

			float sse = data.getSSE( data.normY );
			assertTrue( "SSE should be in [-1,1]", sse > -2 && sse < 2 );
		}
	}

	private List<Data> readData () throws IOException {
		File csvFile = new File( "D:\\My Data\\MS\\CI\\GeneticAlgo\\src\\com\\ci\\geneticalgo\\data.csv" );
		CSVParser parser = CSVParser.parse( csvFile, Charset.defaultCharset(), CSVFormat.EXCEL );

		List<Data> csvData = new ArrayList<>();
		float max = 0, min = 0;
		for (CSVRecord record : parser) {
			if (parser.getRecordNumber() == 1) continue;
			Data data = new Data();

			for (int i = 0; i < 4; i++) {
				//				System.out.print( record.get( i ) + " | " );
				float v = data.setValue( i, record.get( i ) );
				if (v > max) {
					max = v;
				}
				if (v < min || min == 0) {
					min = v;
				}
			}
			csvData.add( data );
		}
		float diff = max - min;
		for (Data data : csvData) {
			data.normalize( min, diff );
		}
		return csvData;
	}

	@Test
	public void randomFloatTest () {
		for (int i = 0; i < 10000; i++) {
			float v = Utils.randomFloatBetween( -5f, 5f );
			assertTrue( "Float:" + v, v >= -5f && v <= 5f );
		}
	}

	@Test
	public void randomIntTest () {
		for (int i = 0; i < 10000; i++) {
			int v = randomIntBetween( 1, 7 );
			assertTrue( "Integer:" + v, v >= 1 && v <= 7 );
		}
	}

	private int randomIntBetween (int min, int max) {
		Random random = new Random();
		int i = random.nextInt( max - min );
		return i + min;
	}

	@Test
	public void testGaussian () {
		for (int i = 0; i < 1000; i++) {
			int min = -2;
			int max = 2;
			float value = Utils.gaussianBetweenMinMax( min, max );
			assertTrue( "Float:" + value, value >= min && value <= max );
		}
	}

	@Test
	public void initialization () {

		for (int i = 0; i < 1000; i++) {
			Quadratic point = Quadratic.initRandom();
			assertTrue( "Value" + point.x, point.x <= 5f );
			assertTrue( "Value" + point.x, point.x >= -5f );
			assertTrue( "Value" + point.y, point.y <= 5f );
			assertTrue( "Value" + point.y, point.y >= -5f );
		}
	}
}
